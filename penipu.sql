/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : penipu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-08-28 09:37:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(50) unsigned NOT NULL DEFAULT '0',
  `post_id` int(50) unsigned NOT NULL DEFAULT '0',
  `parent` int(255) unsigned NOT NULL DEFAULT '0',
  `content` text COLLATE utf8mb4_unicode_ci,
  `deleted` smallint(10) unsigned NOT NULL DEFAULT '0',
  `delete_by_user` smallint(10) unsigned NOT NULL DEFAULT '0',
  `delete_by_admin` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_post_foreign` (`post_id`),
  KEY `comments_user_foreign` (`user_id`) USING BTREE,
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('23', '1', '6', '0', 'Komentear', '1', '1', '0', '2018-08-27 01:13:21', '2018-08-27 01:14:18');
INSERT INTO `comments` VALUES ('24', '1', '6', '0', 'tes', '1', '1', '0', '2018-08-27 01:14:30', '2018-08-27 01:14:33');
INSERT INTO `comments` VALUES ('25', '1', '6', '0', 'tess', '1', '1', '0', '2018-08-27 01:16:58', '2018-08-27 01:18:44');
INSERT INTO `comments` VALUES ('26', '1', '6', '0', 'asd', '0', '0', '0', '2018-08-27 01:18:47', '2018-08-27 01:18:47');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2018_06_01_154211_entrust_setup_tables', '1');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(171) NOT NULL,
  `slug` varchar(171) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('1', 'About', 'about', '&lt;p&gt;asd asd sa dad ad ad asda sd&amp;nbsp;&lt;/p&gt;', '0', '2018-08-24 22:30:38', '2018-08-24 22:55:31');
INSERT INTO `pages` VALUES ('2', 'Syarat & Ketentuan', 'syarat-ketentuan', '&lt;p&gt;Syarat &amp;amp; Ketentuan&lt;/p&gt;', '1', '2018-08-24 23:29:00', '2018-08-24 23:29:00');
INSERT INTO `pages` VALUES ('3', 'Disclaimer', 'disclaimer', '&lt;p&gt;Disclaimer&lt;/p&gt;', '1', '2018-08-24 23:29:11', '2018-08-24 23:29:11');
INSERT INTO `pages` VALUES ('4', 'Tentang Layanan', 'tentang-layanan', '&lt;p&gt;Tentang Layanan&lt;/p&gt;', '1', '2018-08-24 23:29:25', '2018-08-24 23:29:25');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'create-post', 'Mengirim Laporan', 'Membuat/mengirim laporan', null, null);
INSERT INTO `permissions` VALUES ('2', 'post-comment', 'Mengirim Komentar', 'Mengomentari postingan', null, null);

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(50) unsigned NOT NULL DEFAULT '0',
  `anonym` smallint(10) unsigned NOT NULL DEFAULT '0',
  `nama` text COLLATE utf8mb4_unicode_ci,
  `rekening_bank` text COLLATE utf8mb4_unicode_ci,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `nomor_telepon` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `media_sosial` text COLLATE utf8mb4_unicode_ci,
  `jumlah_kerugian` int(50) unsigned NOT NULL DEFAULT '0',
  `screenshot` text COLLATE utf8mb4_unicode_ci,
  `kronologi` text COLLATE utf8mb4_unicode_ci,
  `izinkan_komentar` smallint(10) unsigned NOT NULL DEFAULT '1',
  `status` smallint(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_foreign` (`user_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('6', '1', '0', '[\"Rony Wisnu Wardana\",\"Rizky Puji R\",\"Nama ke 3\"]', '[{\"nama_bank\":\"BCA\",\"nomor_rekening\":\"2890598598\",\"nama_pemilik\":\"Rony Wisnu Wardana\"},{\"nama_bank\":\"BNI\",\"nomor_rekening\":\"042366653\",\"nama_pemilik\":\"Rizky Puji Raharja\"}]', 'Jl. Sukoasri RT/RW 001/001 Dukuh Krajan Desa Poko Kecamatan Jambon', '[\"085338340449\",\"089916666277\"]', '[\"ronywisnuwardana@gmail.com\",\"rizky.rsj@gmail.com\"]', '[\"https:\\/\\/facebook.com\\/fffff\",\"https:\\/\\/facebook.com\\/abcd\"]', '70000', '[\"15345145695b76d589e96ec3855254fa1eeb534e3131155d9c242ba.png\"]', '&lt;p&gt;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun&lt;/p&gt;', '1', '2', '2018-07-12 17:42:04', '2018-08-17 14:02:49');
INSERT INTO `posts` VALUES ('7', '1', '0', '[\"Rony Wisnu Wardana\",\"Rizky Puji R\",\"Nama ke 3\"]', '[{\"nama_bank\":\"BCA\",\"nomor_rekening\":\"2890598598\",\"nama_pemilik\":\"Rony Wisnu Wardana\"},{\"nama_bank\":\"BNI\",\"nomor_rekening\":\"042366653\",\"nama_pemilik\":\"Rizky Puji Raharja\"}]', 'Jl. Sukoasri RT/RW 001/001 Dukuh Krajan Desa Poko Kecamatan Jambon', '[\"085338340449\",\"089916666277\"]', '[\"ronywisnuwardana@gmail.com\",\"rizky.rsj@gmail.com\"]', '[\"https:\\/\\/facebook.com\\/fffff\",\"https:\\/\\/facebook.com\\/abcd\"]', '70000', '[\"15345145695b76d589e96ec3855254fa1eeb534e3131155d9c242ba.png\"]', '&lt;p&gt;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun&lt;/p&gt;', '1', '2', '2018-07-12 17:42:04', '2018-08-17 14:02:49');
INSERT INTO `posts` VALUES ('9', '1', '0', '[\"Rony Wisnu Wardana\",\"Rizky Puji R\",\"Nama ke 3\"]', '[{\"nama_bank\":\"BCA\",\"nomor_rekening\":\"2890598598\",\"nama_pemilik\":\"Rony Wisnu Wardana\"},{\"nama_bank\":\"BNI\",\"nomor_rekening\":\"042366653\",\"nama_pemilik\":\"Rizky Puji Raharja\"}]', 'Jl. Sukoasri RT/RW 001/001 Dukuh Krajan Desa Poko Kecamatan Jambon', '[\"085338340449\",\"089916666277\"]', '[\"ronywisnuwardana@gmail.com\",\"rizky.rsj@gmail.com\"]', '[\"https:\\/\\/facebook.com\\/fffff\",\"https:\\/\\/facebook.com\\/abcd\"]', '70000', '[\"15345145695b76d589e96ec3855254fa1eeb534e3131155d9c242ba.png\"]', '&lt;p&gt;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun&lt;/p&gt;', '1', '2', '2018-07-12 17:42:04', '2018-08-17 14:02:49');
INSERT INTO `posts` VALUES ('10', '1', '0', '[\"Rony Wisnu Wardana\",\"Rizky Puji R\",\"Nama ke 3\"]', '[{\"nama_bank\":\"BCA\",\"nomor_rekening\":\"2890598598\",\"nama_pemilik\":\"Rony Wisnu Wardana\"},{\"nama_bank\":\"BNI\",\"nomor_rekening\":\"042366653\",\"nama_pemilik\":\"Rizky Puji Raharja\"}]', 'Jl. Sukoasri RT/RW 001/001 Dukuh Krajan Desa Poko Kecamatan Jambon', '[\"085338340449\",\"089916666277\"]', '[\"ronywisnuwardana@gmail.com\",\"rizky.rsj@gmail.com\"]', '[\"https:\\/\\/facebook.com\\/fffff\",\"https:\\/\\/facebook.com\\/abcd\"]', '70000', '[\"15345145695b76d589e96ec3855254fa1eeb534e3131155d9c242ba.png\"]', '&lt;p&gt;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun&lt;/p&gt;', '1', '2', '2018-07-12 17:42:04', '2018-08-17 14:02:49');
INSERT INTO `posts` VALUES ('11', '1', '0', '[\"Haha\"]', '[{\"nama_bank\":\"BCA\",\"nomor_rekening\":\"2890598598\",\"nama_pemilik\":\"Rony Wisnu Wardana\"},{\"nama_bank\":\"BNI\",\"nomor_rekening\":\"042366653\",\"nama_pemilik\":\"Rizky Puji Raharja\"}]', 'Jl. Sukoasri RT/RW 001/001 Dukuh Krajan Desa Poko Kecamatan Jambon', '[\"085338340449\",\"089916666277\"]', '[\"ronywisnuwardana@gmail.com\",\"rizky.rsj@gmail.com\"]', '[\"https:\\/\\/facebook.com\\/fffff\",\"https:\\/\\/facebook.com\\/abcd\"]', '70000', '[\"15345145695b76d589e96ec3855254fa1eeb534e3131155d9c242ba.png\"]', '&lt;p&gt;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun.&amp;nbsp;Konten dalam postingan ini dikirimkan oleh dan menjadi tanggung jawab pengirim serta mewakili dalam hal dan kepentingan apapun&lt;/p&gt;', '1', '2', '2018-07-12 17:42:04', '2018-08-17 14:02:49');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('3', 'member', 'Member', 'Member', '2018-06-01 17:09:14', '2018-06-01 17:09:14');
INSERT INTO `roles` VALUES ('4', 'admin', 'Administrator', 'Administrator', null, null);

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES ('1', '4');
INSERT INTO `role_user` VALUES ('6', '3');
INSERT INTO `role_user` VALUES ('7', '3');

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(171) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_json` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'site_name', 'Penipu.co.id', '0', null, null);
INSERT INTO `settings` VALUES ('2', 'site_tagline', 'Daftar Penipu Terlengkap by P-Store.Net', '0', null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_member` smallint(10) unsigned NOT NULL DEFAULT '0',
  `verified_email` smallint(10) unsigned NOT NULL DEFAULT '0',
  `timezone` enum('Asia/Jakarta','Asia/Makassar','Asia/Jayapura') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Jakarta',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Administrator', 'terusberkreasi@gmail.com', '$2y$10$qQsGjYNTD/ukOY7fIAQx/OzsJUPpcNUMzZJXpkeo94eDOXMQp81sa', 'Dsn. Ngompak 2 RT 003 RW 003 Ds. Cepoko Kec. Ngrambe', 'Ngawi', 'Jawa Timur', '63263', '1', '1', 'Asia/Jakarta', 'EjKG9UF3BroswRy7NAaNM3K2vEmFRH9Ls0W8upTORKmyv8Et2i07qhuqyS04', '2018-06-01 16:32:06', '2018-08-20 06:52:45');
INSERT INTO `users` VALUES ('6', 'Rony Wisnu Wardana', 'ronywisnuwardana@gmail.com', '$2y$10$qQsGjYNTD/ukOY7fIAQx/OzsJUPpcNUMzZJXpkeo94eDOXMQp81sa', null, null, null, null, '1', '0', 'Asia/Jakarta', '3ojOVDndjozkXZTWreMImVGskP1F0xmVjzfRO3V6d0g0lUj0HTSs5RnhbUC8', '2018-06-01 17:09:14', '2018-06-01 18:30:43');
INSERT INTO `users` VALUES ('7', 'Rizky Puji Raharja', 'rizky.rsj@gmail.com', '$2y$10$CUnNeFMbj2BfG9PA7d4V..7GHSPloE4B.ajyBoW0VbpJ60Ar0lRQK', 'Sumedang', 'Sumedang', 'Jawa Barat', '45364', '0', '0', 'Asia/Jakarta', 'q6VR6AXbTHj73xD9MVvIoICqLvXbiRiMi2vRyIbkJzkUVa4kHD2zCXi6dAh8', '2018-08-15 16:21:44', '2018-08-19 01:11:38');

-- ----------------------------
-- Table structure for user_verifications
-- ----------------------------
DROP TABLE IF EXISTS `user_verifications`;
CREATE TABLE `user_verifications` (
  `user_id` int(50) unsigned NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(10) unsigned NOT NULL DEFAULT '0',
  `additional_info` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `user_verifications_user_foreign` (`user_id`),
  CONSTRAINT `user_verifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_verifications
-- ----------------------------
INSERT INTO `user_verifications` VALUES ('1', '15b7a61dfd3a3de68ab3b6fdedeb44db7e4626bbe22b6e890.png', '15b7a61dfd3d52e68ab3b6fdedeb44db7e4626bbe22b6e540.png', '1', 'Terima kasih sudah melakukan verifikasi', '2018-08-20 06:38:23', '2018-08-20 06:59:14');
