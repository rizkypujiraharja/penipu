@extends('layouts.member')
@section('title', 'Dashboard')
@section('css')
<style>
	#dashboard_listing_blcok .statusbox .statusbox-content .ic_col_one i, #dashboard_listing_blcok .statusbox .statusbox-content .ic_col_two i, #dashboard_listing_blcok .statusbox .statusbox-content .ic_col_three i, #dashboard_listing_blcok .statusbox .statusbox-content .ic_col_four i {
		background: transparent !important;
	}
	#vfx-search-box select.form-control, #vfx-search-box input.form-control, #vfx-search-btn button {
		box-shadow: none !important
	}
	.tg-listing-head .tg-titlebox {
		padding: 10px !important;
	}
</style>
@endsection
@section('breadcrumb')
<div id="breadcrum-inner-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="breadcrum-inner-header">
          <h1>Dashboard</h1> 
          <a href="#">Dashboard</a>
      	</div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('member-content')
<div id="dashboard_listing_blcok">
    <div class="col-md-3 col-sm-6">
        <div class="statusbox">
            <h3>Total Laporan</h3>
            <div class="statusbox-content">
               	<p class="ic_status_item ic_col_one"><i class="fa fa-file-o"></i></p>
                <h2>{{ number_format_short($totalCount) }}</h2>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="statusbox">
            <h3>Dalam Review</h3>
            <div class="statusbox-content">
                <p class="ic_status_item ic_col_three"><i class="fa fa-balance-scale"></i></p>
                <h2>{{ number_format_short($reviewCount) }}</h2>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="statusbox">
            <h3>Diterima</h3>
            <div class="statusbox-content">
                <p class="ic_status_item ic_col_four"><i class="fa fa-calendar-check-o"></i></p>
                <h2>{{ number_format_short($approvedCount) }}</h2>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="statusbox">
            <h3>Ditolak</h3>
            <div class="statusbox-content">
                <p class="ic_status_item ic_col_two"><i class="fa fa-calendar-times-o"></i></p>
                <h2>{{ number_format_short($rejectedCount) }}</h2>
            </div>
        </div>
    </div>
	<div class="col-md-12">
        <form id="search-form" action="">
          	<div class="col-sm-9 col-md-10 nopadding">
            	<div id="vfx-search-box">
              		<div class="col-sm-3 nopadding">
                		<select id="search-location" class="form-control" name="status">
                  			<option value="all" {{ (Request::input('status') == 'all' ? 'selected' : '') }}>Semua Kategori</option>
                        <option value="0" {{ (Request::input('status') == '0' ? 'selected' : '') }}>Draft</option>
                  			<option value="1" {{ (Request::input('status') == '1' ? 'selected' : '') }}>Dalam Review</option>
                  			<option value="2" {{ (Request::input('status') == '2' ? 'selected' : '') }}>Diterima</option>
                  			<option value="3" {{ (Request::input('status') == '3' ? 'selected' : '') }}>Ditahan</option>
                        <option value="4" {{ (Request::input('status') == '4' ? 'selected' : '') }}>Ditolak</option>
                		</select>
              		</div>
              		<div class="col-sm-3 nopadding">
                		<select id="search-location2" class="form-control" name="search_by">
                  			<option value="all">Berdasarkan</option>
                  			<option value="all" {{ (Request::input('search_by') == 'all' ? 'selected' : '') }}>Semua</option>
                  			<option value="nama" {{ (Request::input('search_by') == 'nama' ? 'selected' : '') }}>Nama</option>
                  			<option value="nomor_rekening" {{ (Request::input('search_by') == 'nomor_rekening' ? 'selected' : '') }}>Nomor Rekening</option>
                  			<option value="nomor_telepon" {{ (Request::input('search_by') == 'nomor_telepon' ? 'selected' : '') }}>Nomor Telepon/HP</option>
                  			<option value="email" {{ (Request::input('search_by') == 'email' ? 'selected' : '') }}>Email</option>
                  			<option value="media_sosial" {{ (Request::input('search_by') == 'media_sosial' ? 'selected' : '') }}>Akun Medsos</option>
                		</select>
              		</div>
              		<div class="col-sm-6 nopadding">
                		<div class="form-group">
                  			<input id="search-data" class="form-control" name="keyword" placeholder="Kata kunci" value="{{ Request::input('keyword') }}">
                		</div>
              		</div>
            	</div>
          	</div>
          	<div class="col-sm-3 col-md-2 text-right nopadding-right">
            	<div id="vfx-search-btn">
              		<button type="submit" id="search"><i class="fa fa-search"></i>Cari</button>
            	</div>
          	</div>
        </form>
    </div>

    <div class="tg-listing" style="margin-top: 30px;">
        <div class="tg-listing-head">
            <div class="tg-titlebox">
                <h5>Nama</h5>
            </div>
            <div class="tg-titlebox">
                <h5>Tanggal</h5>
            </div>
            <div class="tg-titlebox">
                <h5>Komentar</h5>
            </div>
            <div class="tg-titlebox">
                <h5>Tindakan</h5>
            </div>
        </div>
        <div class="tg-lists">
          @foreach($posts as $post)
            <div class="tg-list">
                <div class="tg-listbox" data-title="title">
                    <div class="tg-listdata">
                    	<h4><a href="#">
                                    @php
                                      $nama = @json_decode($post->nama)[0]
                                    @endphp
                                  {{ $nama }}
                                  </a></h4>
                    	<span class="pull-left"><b>ID Laporan:</b> {{ $post->id }}</span>
                      @php
                      switch ($post->status) {
                        case 0:
                          $class = 'label-default';
                          $status = 'Draft';
                          break;
                        
                        case 1:
                          $class = 'label-info';
                          $status = 'Dalam Review';
                          break;
                        
                        case 2:
                          $class = 'label-success';
                          $status = 'Diterima';
                          break;
                        
                        case 3:
                          $class = 'label-warning';
                          $status = 'Ditahan';
                          break;
                        
                        case 4:
                          $class = 'label-danger';
                          $status = 'Ditolak';
                          break;
                        
                        default:
                          # code...
                          break;
                      }
                      @endphp
                      <br>
                      <span class="pull-left label {{$class}}" style="color: #fff">{{$status}}</span>
                    </div>
                </div>
                <div class="tg-listbox" data-viewed="viewed"> <span><time>{{ userDate($post->created_at,'d-m-Y', $signed_user->timezone) }}</time></span> </div>
                <div class="tg-listbox"> <span>{{ $post->__comments->count() }}</span> </div>
                <div class="tg-listbox" data-action="action"> <a class="tg-btn-list" href="{{ url('/account/report/edit', $post) }}"><i class="fa fa-pencil"></i></a> <a class="tg-btn-list delete" href="#!" data-action="{{ url('/account/report/delete', $post) }}"><i class="fa fa-trash-o"></i></a> </div>
            </div>
          @endforeach
        </div>
        <div class="vfx-person-block">
          <center>
            Page {{ $page }}
          </center>
            <ul class="vfx-pagination">
              <li><a href="{{$request->fullUrlWithQuery(['page' => 1])}}"><i class="fa fa-angle-double-left"></i></a></li>
              @php
                if ($page <= 3 || $jumpage < $page) {
                  $start = 1;
                  if($jumpage < 5){
                    $end = $jumpage;
                  }else{
                    $end = 5;
                  }
                } else if ($jumpage - 2 < $page ) {
                  $start = $jumpage - 4;
                  $end = $jumpage;
                }else{
                  $start = $page - 2;
                  $end = $start + 4;
                }
              @endphp
            @for ($i = $start; $i <= $end; $i++)
              <li class="{{$i == $page ? 'active' : ''}}"><a href="{{$request->fullUrlWithQuery(['page'=>$i])}}">{{$i}}</a></li>
            @endfor
              <li><a href="{{$request->fullUrlWithQuery(['page' => $jumpage])}}"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
    </div> 
</div>

@endsection

@section('js')
<script type="text/javascript">
    $('.delete').on('click', function(){
        var href = $(this).attr('data-action');
        swal({
          title: "Apakah Anda yakin menghapus laporan ini?",
          text: "Setelah dihapus laporan tidak dapat dikembalikan",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location.href = href;
          }
        });
    });
</script>
@endsection