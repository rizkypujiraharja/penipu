@extends('layouts.member')
@section('title', 'Ubah Kata Sandi')
@section('breadcrumb')
<div id="breadcrum-inner-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="breadcrum-inner-header">
          <h1>Ubah Kata Sandi</h1>
          <a href="{{ url('/account/home') }}">Dashboard</a> <i class="fa fa-circle"></i> <a href="#"><span>Ubah Kata Sandi</span></a> </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('member-content')
<div class="submit_listing_box">
    <h3>Ubah Kata Sandi</h3>
    <form class="form-alt" action="" method="POST">
    	@csrf
        <div class="row">
            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                <label>Kata Sandi Saat Ini <span>*</span></label>
                <input placeholder="" required class="form-control {{ $errors->has('password') ? 'field-error' : '' }}" type="password" name="password">
                @if($errors->has('password'))
                	<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('password') }}</div>
                @endif
            </div>
            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                <label>Kata Sandi Baru <span>*</span></label>
                <input placeholder="" required class="form-control {{ $errors->has('new_password') ? 'field-error' : '' }}" type="password" name="new_password">
                @if($errors->has('new_password'))
                	<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('new_password') }}</div>
                @endif
            </div>
            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                <label>Ulangi Kata Sandi Baru <span>*</span></label>
                <input placeholder="" required class="form-control {{ $errors->has('new_password_confirmation') ? 'field-error' : '' }}" type="password" name="new_password_confirmation">
                @if($errors->has('new_password_confirmation'))
                	<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('new_password_confirmation') }}</div>
                @endif
            </div>
            <div class="from-list-lt col-md-12">
                <div class="form-group btn_change_pass">
                    <button class="btn pull-right" type="submit">Save Setting</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection