@extends('layouts.member')
@section('title', 'Edit Profile')
@section('breadcrumb')
<div id="breadcrum-inner-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="breadcrum-inner-header">
          <h1>Edit Profile</h1>
          <a href="{{ url('/account/home') }}">Dashboard</a> <i class="fa fa-circle"></i> <a href="#"><span>Edit Profile</span></a> </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css')
<style type="text/css">
.profile-label {padding:5px 15px;float:left}
</style>
@endsection

@section('member-content')
<div class="submit_listing_box">
    <h3>Edit Profile</h3>
    <form class="form-alt" action="" method="POST">
    	@csrf
        <div class="row">
            <form action="{{ route('account.profile.store') }}" class="form-control">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Nama Lengkap</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ $user->name ?? old('name') }}" {{ $user->verified_member == 1 ? 'readonly' : 'required'}}>
                            @if ($errors->has('name'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Email</label>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ $user->email ?? old('email') }}" required>
                            @if ($errors->has('email'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Alamat</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="alamat" value="{{ $user->alamat ?? old('alamat') }}" {{ $user->verified_member == 1 ? 'readonly' : 'required'}}>
                            @if ($errors->has('alamat'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('alamat') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Kota</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="kota" value="{{ $user->kota ?? old('kota') }}" {{ $user->verified_member == 1 ? 'readonly' : 'required'}}>
                            @if ($errors->has('kota'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('kota') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Provinsi</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="provinsi" value="{{ $user->provinsi ?? old('provinsi') }}" {{ $user->verified_member == 1 ? 'readonly' : 'required'}}>
                            @if ($errors->has('provinsi'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('provinsi') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Kode Pos</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="kode_pos" value="{{ $user->kode_pos ?? old('kode_pos') }}" {{ $user->verified_member == 1 ? 'readonly' : 'required'}}>
                            @if ($errors->has('kode_pos'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('kode_pos') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12 pull-left">
                    <button class="btn btn-primary pull-left" type="submit">Simpan</button>
                </div>
            </form>
        </div>
    </form>
</div>
@endsection