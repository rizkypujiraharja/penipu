@extends('layouts.member')
@section('title', 'Verifikasi Akun')
@section('breadcrumb')
<div id="breadcrum-inner-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="breadcrum-inner-header">
          <h1>Verifikasi Akun</h1>
          <a href="{{ route('account.home') }}">Dashboard</a> <i class="fa fa-circle"></i> <a href="#"><span>Verifikasi Akun</span></a> </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('member-content')
	@if(!is_null($verification))
		<div class="submit_listing_box">
            <h3>STATUS VERIFIKASI : {{ $verification->status == $verification->getConstant('STATUS_REVIEW') ? 'Dalam Review' : ($verification->status == $verification->getConstant('STATUS_APPROVED') ? 'Diterima' : 'Ditolak') }}</h3>
            <div class="form-alt">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12" style="text-align:left">
                    {!! $verification->additional_info !!}
                  </div>
                </div>
          	</div>
        </div>
	@else
		<form action="{{ route('account.verification.store') }}" method="POST" enctype="multipart/form-data" id="verification-form">
			@csrf
			<div class="submit_listing_box">
				<div style="text-align:left;font-size:12px;margin-bottom:20px"><i class="fa fa-info-circle"></i> Kolom yang bertanda bintang (*) wajib diisi</div>
              <h3>IDENTITAS DIRI</h3>
              <div class="form-alt">
              	<div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Nama Lengkap :*</label></div>
              		<div id="nama">
			            <div id="nama-0" class="field">
			              	<div class="form-group col-md-10 col-sm-9 col-xs-8">
			                    <input placeholder="" class="form-control" type="text" name="nama" value="{{ old('nama') ?? $user->name }}" required>
			                    @if(isset(Session::get('form_validation_error')['nama']))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['nama'] }}</div>
				                @endif
			                </div>
			            </div>
                  	</div>
                </div>
                <br/>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Alamat :*</label></div>
                	<div class="form-group col-md-10 col-sm-12 col-xs-12" style="margin-bottom:15px">
	                  	<textarea placeholder="" class="form-control" rows="3" name="alamat" required>{{ old('alamat') ?? $user->alamat }}</textarea>
	                  	@if(isset(Session::get('form_validation_error')['alamat']))
				            <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['alamat'] }}</div>
				    	@endif
	                </div>
	            </div>
	            <br/>
	            <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Kota/Kabupaten :*</label></div>
              		<div id="nomor-telepon">
              			<div id="nomor-telepon-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="text" name="kota" value="{{ old('kota') ?? $user->kota }}">
		                    	@if(isset(Session::get('form_validation_error')['kota']))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['kota'] }}</div>
				                @endif
		                  	</div>
		                </div>
                  	</div>
                </div>
                <br/>
                <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Provinsi :*</label></div>
              		<div id="nomor-telepon">
              			<div id="nomor-telepon-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="text" name="provinsi" value="{{ old('provinsi') ?? $user->provinsi }}">
		                    	@if(isset(Session::get('form_validation_error')['provinsi']))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['provinsi'] }}</div>
				                @endif
		                  	</div>
		                </div>
                  	</div>
                </div>
                <br/>
                <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Kode Pos :*</label></div>
              		<div id="nomor-telepon">
              			<div id="nomor-telepon-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="text" name="kode_pos" value="{{ old('kode_pos') ?? $user->kode_pos }}">
		                    	@if(isset(Session::get('form_validation_error')['kode_pos']))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['kode_pos'] }}</div>
				                @endif
		                  	</div>
		                </div>
                  	</div>
                </div>
              </div>
            </div>
            
            <div class="submit_listing_box">
              <h3>DOKUMEN IDENTITAS (MAKS. 1MB/GAMBAR)</h3>
              <div class="form-alt">
              	<div class="row">
	                <div id="screenshot">
              			<div id="screenshot-0" class="field">
              				<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Foto Setengah Badan :*</label></div>
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="file" name="photo" accept="image/*" required>
		                    	@if(isset(Session::get('form_validation_error')['photo']))
								    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['photo'] }}</div>
								@endif
		                  	</div>
		                </div>
                  	</div>
	            </div>
	            <br/>
	            <div class="row">
	                <div id="screenshot">
              			<div id="screenshot-0" class="field">
              				<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">KTP :*</label></div>
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="file" name="id_document" accept="image/*" required>
		                    	@if(isset(Session::get('form_validation_error')['id_document']))
								    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['id_document'] }}</div>
								@endif
		                  	</div>
		                </div>
                  	</div>
	            </div>
              </div>
            </div>

            <div class="submit_listing_box">
              <h3>PERNYATAAN</h3>
              <div class="form-alt">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12" style="text-align:left">
                    "Dengan mengirimkan permintaan verifikasi akun, saya ({{ $user->name }}), menjamin bahwa data dan dokumen yang dikirimkan diatas adalah BENAR dan menjamin bahwa akan mematuhi Persyaratan Layanan serta menyetujui Kebijakan Privasi yang berlaku"
                  </div>
                </div>
              </div>
            </div>

            <div class="from-list-lt">
              <div class="form-group">
                <button class="btn" type="submit">Kirim</button>
              </div>
            </div>
		</form>
	@endif
@endsection