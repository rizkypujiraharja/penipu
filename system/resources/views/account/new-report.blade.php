@extends('layouts.member')
@section('title', 'Kirim Laporan')
@section('breadcrumb')
<div id="breadcrum-inner-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="breadcrum-inner-header">
          <h1>Kirim Laporan</h1>
          <a href="{{ url('/account/home') }}">Dashboard</a> <i class="fa fa-circle"></i> <a href="#"><span>Kirim Laporan</span></a> </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('member-content')
<form action="{{ route('account.newreport.store') }}" method="POST" enctype="multipart/form-data" id="new-post-form">
	@csrf
			<div class="submit_listing_box">
				<div style="text-align:left;font-size:12px;margin-bottom:20px"><i class="fa fa-info-circle"></i> Kolom yang bertanda bintang (*) wajib diisi</div>
              <h3>IDENTITAS PELAKU</h3>
              <div class="form-alt">
              	<div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Nama Pelaku :*</label></div>
              		<div id="nama">
			            <div id="nama-0" class="field">
			              	<div class="form-group col-md-10 col-sm-9 col-xs-8">
			                    <input placeholder="Nama (wajib)" class="form-control" type="text" name="nama[]" value="{{ old('nama')[0] }}" required>
			                    @if(isset(Session::get('form_validation_error')['nama'][0]))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['nama'][0] }}</div>
				                @endif
			                </div>
			                <div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
			                  	<button onclick="addField('nama');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
			                </div>
			            </div>
			            @if(is_array(old('nama')))
				            @if(count(old('nama')) > 1)
				            	@for($i=1; $i < count(old('nama')); $i++)
				            	<div class="clearfix" id="nama-{{ $i }}-clearfix"></div>
				            	<div id="nama-{{ $i }}" class="field">
					              	<div class="form-group col-md-10 col-sm-9 col-xs-8">
					                    <input placeholder="Nama (wajib)" class="form-control" type="text" name="nama[]" value="{{ old('nama')[$i] }}" required>
					                    @if(isset(Session::get('form_validation_error')['nama'][$i]))
					                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['nama'][$i] }}</div>
					                    @endif
					                </div>
					                <div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
					                  	<button onclick="removeField('nama-{{ $i }}');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button>
					                </div>
					            </div>
					            @endfor
				            @endif
			            @endif
                  	</div>
                </div>
                <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Rekening Bank :*</label></div>
              		<div id="rekening-bank">
              			<div id="rekening-bank-0" class="field">
		              		<div class="form-group col-md-3 col-sm-9 col-xs-12">
		                    	<input placeholder="Nama Bank (wajib)" class="form-control" type="text" name="rekening_bank[nama_bank][]" value="{{ old('rekening_bank')['nama_bank'][0] }}" required>
		                  	</div>
		                  	<div class="form-group col-md-3 col-sm-9 col-xs-12">
		                    	<input placeholder="Nomor Rekening (wajib)" class="form-control" type="text" name="rekening_bank[nomor_rekening][]" value="{{ old('rekening_bank')['nomor_rekening'][0] }}" required>
		                  	</div>
		                  	<div class="form-group col-md-4 col-sm-9 col-xs-8">
		                    	<input placeholder="Nama Pemilik (wajib)" class="form-control" type="text" name="rekening_bank[nama_pemilik][]" value="{{ old('rekening_bank')['nama_pemilik'][0] }}" required>
		                  	</div>
		                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
		                  		<button onclick="addField('rekening-bank');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
		                  	</div>
		                  	@if(isset(Session::get('form_validation_error')['rekening_bank'][0]))
				                <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['rekening_bank'][0] }}</div>
				            @endif
		                </div>
		                @if(is_array(old('rekening_bank')['nama_bank']))
				            @if(count(old('rekening_bank')['nama_bank']) > 1)
				            	@for($i=1; $i < count(old('rekening_bank')['nama_bank']); $i++)
				            	<div class="clearfix" id="rekening-bank-{{ $i }}-clearfix"></div>
				            	<div id="rekening-bank-{{ $i }}" class="field">
				              		<div class="form-group col-md-3 col-sm-9 col-xs-12">
				                    	<input placeholder="Nama Bank (wajib)" class="form-control" type="text" name="rekening_bank[nama_bank][]" value="{{ old('rekening_bank')['nama_bank'][$i] }}" required>
				                  	</div>
				                  	<div class="form-group col-md-3 col-sm-9 col-xs-12">
				                    	<input placeholder="Nomor Rekening (wajib)" class="form-control" type="text" name="rekening_bank[nomor_rekening][]" value="{{ old('rekening_bank')['nomor_rekening'][$i] }}" required>
				                  	</div>
				                  	<div class="form-group col-md-4 col-sm-9 col-xs-8">
				                    	<input placeholder="Nama Pemilik (wajib)" class="form-control" type="text" name="rekening_bank[nama_pemilik][]" value="{{ old('rekening_bank')['nama_pemilik'][$i] }}" required>
				                  	</div>
				                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
				                  		<button onclick="removeField('rekening-bank-{{ $i }}');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button>
				                  	</div>
				                  	@if(isset(Session::get('form_validation_error')['rekening_bank'][$i]))
						                <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['rekening_bank'][$i] }}</div>
						            @endif
				                </div>
				            	@endfor
				            @endif
				        @endif
                  	</div>
                </div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Alamat :</label></div>
                	<div class="form-group col-md-12 col-sm-12 col-xs-12" style="margin-bottom:15px">
	                  	<textarea placeholder="Biarkan kosong jika tidak ada" class="form-control" rows="3" name="alamat">{{ old('alamat') }}</textarea>
	                  	@if(isset(Session::get('form_validation_error')['alamat']))
				            <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['alamat'] }}</div>
				    	@endif
	                </div>
	            </div>
	            <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Nomor Telepon/HP :</label></div>
              		<div id="nomor-telepon">
              			<div id="nomor-telepon-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="(opsional)" class="form-control" type="text" name="nomor_telepon[]" value="{{ old('nomor_telepon')[0] }}">
		                    	@if(isset(Session::get('form_validation_error')['nomor_telepon'][0]))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['nomor_telepon'][0] }}</div>
				                @endif
		                  	</div>
		                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
		                  		<button onclick="addField('nomor-telepon');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
		                  	</div>
		                </div>
		                @if(is_array(old('nomor_telepon')))
				            @if(count(old('nomor_telepon')) > 1)
				            	@for($i=1; $i < count(old('nomor_telepon')); $i++)
				            	<div class="clearfix" id="nomor-telepon-{{ $i }}-clearfix"></div>
				            	<div id="nomor-telepon-{{ $i }}" class="field">
				              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
				                    	<input placeholder="(opsional)" class="form-control" type="text" name="nomor_telepon[]" value="{{ old('nomor_telepon')[$i] }}">
				                    	@if(isset(Session::get('form_validation_error')['nomor_telepon'][$i]))
						                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['nomor_telepon'][$i] }}</div>
						                @endif
				                  	</div>
				                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
				                  		<button onclick="removeField('nomor-telepon-{{ $i }}');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button>
				                  	</div>
				                </div>
				               @endfor
				            @endif
				        @endif
                  	</div>
                </div>
                <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Email :</label></div>
              		<div id="email">
              			<div id="email-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="(opsional)" class="form-control" type="email" name="email[]" value="{{ old('email')[0] }}">
		                    	@if(isset(Session::get('form_validation_error')['email'][0]))
						            <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['email'][0] }}</div>
						        @endif
		                  	</div>
		                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
		                  		<button onclick="addField('email');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
		                  	</div>
		                </div>
		                @if(is_array(old('email')))
				            @if(count(old('email')) > 1)
				            	@for($i=1; $i < count(old('email')); $i++)
				            	<div class="clearfix" id="email-{{ $i }}-clearfix"></div>
				            	<div id="email-{{ $i }}" class="field">
				              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
				                    	<input placeholder="(opsional)" class="form-control" type="email" name="email[]" value="{{ old('email')[$i] }}">
				                    	@if(isset(Session::get('form_validation_error')['email'][$i]))
								            <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['email'][$i] }}</div>
								        @endif
				                  	</div>
				                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
				                  		<button onclick="removeField('email-{{ $i }}');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button>
				                  	</div>
				                </div>
				                @endfor
				            @endif
				        @endif
                  	</div>
                </div>
                <div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Akun Media Sosial :</label></div>
              		<div id="media-sosial">
              			<div id="media-sosial-0" class="field">
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="(opsional)" class="form-control" type="text" value="{{ old('media_sosial')[0] }}" name="media_sosial[]">
		                    	@if(isset(Session::get('form_validation_error')['media_sosial'][0]))
				                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['media_sosial'][0] }}</div>
				                @endif
		                  	</div>
		                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
		                  		<button onclick="addField('media-sosial');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
		                  	</div>
		                </div>
		                @if(is_array(old('media_sosial')))
				            @if(count(old('media_sosial')) > 1)
				            	@for($i=1; $i < count(old('media_sosial')); $i++)
				            	<div class="clearfix" id="media-sosial-{{ $i }}-clearfix"></div>
				            	<div id="media-sosial-{{ $i }}" class="field">
				              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
				                    	<input placeholder="(opsional)" class="form-control" type="text" value="{{ old('media_sosial')[$i] }}" name="media_sosial[]">
				                    	@if(isset(Session::get('form_validation_error')['media_sosial'][$i]))
						                    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['media_sosial'][$i] }}</div>
						                @endif
				                  	</div>
				                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
				                  		<button onclick="removeField('media-sosial-{{ $i }}');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button>
				                  	</div>
				                </div>
				            	@endfor
				            @endif
				        @endif
                  	</div>
                </div>
              </div>
            </div>
            <div class="submit_listing_box">
              <h3>LAIN-LAIN</h3>
              <div class="form-alt">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label style="font-size:16px;font-weight:500">Jumlah Kerugian/Potensi Kerugian (Rp) :*</label>
                    <input placeholder="(wajib)" class="form-control" type="number" value="{{ old('jumlah_kerugian') }}" name="jumlah_kerugian" required>
                    @if(isset(Session::get('form_validation_error')['jumlah_kerugian']))
						<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['jumlah_kerugian'] }}</div>
					@endif
                  </div>
                </div>
              </div>
            </div>
            <div class="submit_listing_box">
              <h3>KRONOLOGI & BUKTI PENDUKUNG</h3>
              <div class="form-alt">
              	<div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Kronologi kasus (min. 300 karakter) :*</label></div>
	                <div class="form-group col-md-12 col-sm-12 col-xs-12">
	                  	<textarea placeholder="(wajib)" class="form-control" rows="5" name="kronologi">{!! old('kronologi') !!}</textarea>
	                  	@if(isset(Session::get('form_validation_error')['kronologi']))
						    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['kronologi'] }}</div>
						@endif
	                </div>
	                <div class="clearfix"></div>
	                <div id="screenshot">
              			<div id="screenshot-0" class="field">
              				<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Maks. 1 Mb/gambar :*</label></div>
		              		<div class="form-group col-md-10 col-sm-9 col-xs-8">
		                    	<input placeholder="" class="form-control" type="file" name="screenshot[]" accept="image/*" required>
		                    	@if(isset(Session::get('form_validation_error')['screenshot']))
								    <div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ Session::get('form_validation_error')['screenshot'] }}</div>
								@endif
		                  	</div>
		                  	<div class="col-md-2 col-sm-3 col-xs-4 from-list-lt">
		                  		<button onclick="addField('screenshot');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-plus-square fa-lg"></i></button>
		                  	</div>
		                </div>
                  	</div>
	            </div>
              </div>
            </div>
            <div class="from-list-lt">
              <div class="form-group">
                <button class="btn" type="submit">Kirim Laporan</button>
              </div>
            </div>
</form>
@endsection

@section('js')
<script src="{{ asset('/assets/vendor/wysiwyg/js/tinymce.min.js') }}"></script>

<script>
'use strict';
$(document).ready(function() {
      tinymce.init({
          selector: 'textarea[name=kronologi]',
          height: 200,
          theme: 'modern',
          plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
          ],
          toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
          image_advtab: true
      });
 });

	function addField(id) {
		if( id == 'nama' ) {
			var count = parseInt($("div#nama").find(".field").length);
			var nextId = count;
			$("div#nama").append('<div class="clearfix" id="nama-' + nextId + '-clearfix"></div><div id="nama-' + nextId + '" class="field"><div class="form-group col-md-10 col-sm-9 col-xs-8"><input placeholder="Nama Alias" class="form-control" type="text" name="nama[]" required></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'nama-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
		}
		else if( id == 'rekening-bank' ) {
			var count = parseInt($("div#rekening-bank").find(".field").length);
			var nextId = count;
			$("div#rekening-bank").append('<div class="clearfix" id="rekening-bank-' + nextId + '-clearfix"></div><div id="rekening-bank-' + nextId + '" class="field"><div class="form-group col-md-3 col-sm-9 col-xs-12"><input placeholder="Nama Bank ' + nextId + '" class="form-control" type="text" name="rekening_bank[nama_bank][]" required></div><div class="form-group col-md-3 col-sm-9 col-xs-12"><input placeholder="Nomor Rekening ' + nextId + '" class="form-control" type="text" name="rekening_bank[nomor_rekening][]" required></div><div class="form-group col-md-4 col-sm-9 col-xs-8"><input placeholder="Nama Pemilik ' + nextId + '" class="form-control" type="text" name="rekening_bank[nama_pemilik][]" required></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'rekening-bank-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
		}
		else if( id == 'nomor-telepon' ) {
			var count = parseInt($("div#nomor-telepon").find(".field").length);
			var nextId = count;
			$("div#nomor-telepon").append('<div class="clearfix" id="nomor-telepon-' + nextId + '-clearfix"></div><div id="nomor-telepon-' + nextId + '" class="field"><div class="form-group col-md-10 col-sm-9 col-xs-8"><input placeholder="Nomor Telepon/HP" class="form-control" type="text" name="nomor_telepon[]"></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'nomor-telepon-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
		}
		else if( id == 'email' ) {
			var count = parseInt($("div#email").find(".field").length);
			var nextId = count;
			$("div#email").append('<div class="clearfix" id="email-' + nextId + '-clearfix"></div><div id="email-' + nextId + '" class="field"><div class="form-group col-md-10 col-sm-9 col-xs-8"><input placeholder="Email" class="form-control" type="email" name="email[]"></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'email-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
		}
		else if( id == 'media-sosial' ) {
			var count = parseInt($("div#media-sosial").find(".field").length);
			var nextId = count;
			$("div#media-sosial").append('<div class="clearfix" id="media-sosial-' + nextId + '-clearfix"></div><div id="media-sosial-' + nextId + '" class="field"><div class="form-group col-md-10 col-sm-9 col-xs-8"><input placeholder="Akun media sosial" class="form-control" type="text" name="media_sosial[]"></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'media-sosial-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
		}
		else if( id == 'screenshot' ) {
			var count = parseInt($("div#screenshot").find(".field").length);
			var nextId = count;
			if( nextId <= 4 ) {
				$("div#screenshot").append('<div class="clearfix" id="screenshot-' + nextId + '-clearfix"></div><div id="screenshot-' + nextId + '" class="field"><div class="form-group col-md-10 col-sm-9 col-xs-8"><input placeholder="" class="form-control" type="file" name="screenshot[]" accept="image/*" required></div><div class="col-md-2 col-sm-3 col-xs-4 from-list-lt"><button onclick="removeField(\'screenshot-' + nextId + '\');" class="btn" type="button" style="height: 43px;border-radius:4px;box-shadow: none"><i class="fa fa-minus-square fa-lg"></i></button></div></div>');
			}
		}
	}

	function removeField(id) {
		$("#" + id).remove();
		$("#" + id + "-clearfix").remove();
	}
</script>
@endsection