@extends('layouts.app')
@section('content')
<div id="location-map-block">
  <div id="location-homemap-block"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-lg-12">
        <div id="location-link-item">
          <button id="map_list"><i class="fa fa-angle-double-up"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="slider-banner-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div id="home-slider-item"> <span class="helpyou_item">Cegah <span>Kejahatan</span> dunia maya
          <p>Cek atau cari informasi tentang pihak tertentu berpotensi melakukan <span>penipuan</span> atau tidak</p>
        </div>
        <div id="search-categorie-item-block" style="margin-bottom:40px !important">
          <div id="categorie-search-form">
            <div class="col-sm-9 col-md-10 nopadding">
              <div id="search-input">
                <div class="col-sm-3 nopadding">
                  <select id="search_by" class="form-control">
                    <option value="">Cari Berdasarkan</option>
                    <option value="nama">Nama</option>
                    <option value="rekening">Nomor Rekening</option>
                    <option value="telepon">Nomor HP/Telepon</option>
                    <option value="email">Email</option>
                    <option value="medsos">Akun Medsos</option>
                  </select>
                </div>
                <div class="col-sm-9 nopadding">
                  <div class="form-group">
                    <input id="keyword" class="form-control" placeholder="Masukkan kata kunci pencarian">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-md-2 text-right nopadding-right">
              <div id="location-search-btn">
                <button type="button" id="search-btn" onclick="search();"><i class="fa fa-search"></i>Cari</button>
              </div>
            </div>
          </div>
        </div>
        <div style="text-align:center;margin-bottom:115px;color:white">
          <p>Disclaimer : Data yang Anda cari mungkin belum tersedia di sistem kami atau masih dalam tahap review dan TIDAK menjamin bahwa pihak tersebut aman dari tindakan kriminal. Semua keputusan bertransaksi adalah independen. Situs ini hanya menampilkan arsip laporan yang pernah dikirimkan para pengguna dan menjadi tanggung jawab masing-masing pengirim.</p>
        </div>
        <div id="location_slider_item_block">
          <button id="map_mark"><i class="fa fa-map-marker"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="vfx-counter-block" style="background:#ffffff !important">
  <div class="vfx-item-container-slope vfx-item-bottom-slope vfx-item-left-slope"></div>
  <div class="container">
    <div class="vfx-item-counter-up">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="vfx-item-countup" style="width:100%;">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-file"></i></div>
            <div id="count-1" class="count_number vfx-item-count-up">{{ number_format_short($postCount) }}</div>
            <div class="counter_text">Laporan Diterima</div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="vfx-item-countup" style="width:100%;">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-ban"></i></div>
            <div id="count-2" class="count_number vfx-item-count-up">{{ number_format_short($blackListCount) }}</div>
            <div class="counter_text">Rekening Terblacklist</div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="vfx-item-countup" style="width:100%;">
            <div class="vfx-item-black-top-arrow"><i class="fa fa-money"></i></div>
            <div id="count-3" class="count_number vfx-item-count-up">Rp {{ number_format_short($kerugian) }}</div>
            <div class="counter_text">Kerugian Tercatat</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>

    $(document).ready(function() {
        window.addEventListener("keydown", function (event) {
            if (event.defaultPrevented) {
                return; // Should do nothing if the default action has been cancelled
            }
            var x = event.which || event.keyCode;
            if( x == '13' || x == 13 ) {
                if( $('#keyword').is(":focus") ) {
                    var searchBy = $("#search_by option:selected").val();
                    var keyword = $("#keyword").val();

                    if( searchBy != '' && keyword != '' ) {
                        search();
                    }
                }
            }
        });
    });

    $("#search_by").change(function() {
        var sb = $("option:selected", this).val();
        if( sb == 'nama' ) {
            $("#keyword").attr("placeholder", "Masukkan nama yang ingin anda cek disini");
        }
        else if( sb == 'rekening' ) {
            $("#keyword").attr("placeholder", "Masukkan nomor rekening yang ingin anda cek disini");
        }
        else if( sb == 'telepon' ) {
            $("#keyword").attr("placeholder", "Masukkan nomor telepon / HP yang ingin anda cek disini");
        }
        else if( sb == 'email' ) {
            $("#keyword").attr("placeholder", "Masukkan email yang ingin anda cek disini");
        }
        else if( sb == 'medsos' ) {
            $("#keyword").attr("placeholder", "Masukkan link profil atau username akun media sosial yang ingin anda cek disini");
        }
        else {
            $("#keyword").attr("placeholder", "Masukkan kata kunci pencarian disini");
        }
    });

    function search() {
        var searchBy = $("#search_by option:selected").val();
        var keyword = $("#keyword").val();
        
        if( searchBy == '' ) {
            alert('Pilih kategori pencarian!');
        }
        else if( keyword == '' ) {
            alert('Masukkan kata kunci pencarian!');
        }
        else {

            var oldSearchButton = $("#search-btn").html();

            $('#search-btn').html('<i class="fa fa-spinner spinning fa-lg"></i>');

            $.ajax({
                url: "{{ route('home.ajax-search') }}",
                type: "POST",
                dataType: "JSON",
                data: {
                    search_by: searchBy,
                    keyword: keyword
                },
                success: function(s) {

                    $('#search-btn').html(oldSearchButton);

                    if( s.success === false ) {
                        alert(s.message);
                    }
                    else if( s.success === true ) {
                        if( s.result_count > 0 ) {
                            window.location.assign("{{ url('/search') }}?by=" + searchBy + "&keyword=" + keyword);
                        }
                        else {
                            swal({
                                title: 'Tidak ditemukan',
                                text: "Data yang anda cari tidak ditemukan di sistem kami, cobalah menggunakan kata kunci yang lebih spesifik/singkat. Mengalami penipuan oleh pihak ini ? Laporkan sekarang!",
                                buttons: {
                                    cancel: {
                                        text: "Tutup",
                                        value: true,
                                        visible: true,
                                        className: "btn btn-danger m-l-10",
                                        closeModal: true,
                                    },
                                    confirm: {
                                        text: "Kirim Laporan",
                                        value: true,
                                        visible: true,
                                        className: "btn btn-primary",
                                        closeModal: false
                                    }
                                },
                            })
                            .then((confirm) => {
                                if (confirm) {
                                    window.location.href = '{{ route('account.newreport') }}';
                                }
                            });
                        }
                    }
                },
                error: function(e) {
                    $('#search-btn').html(oldSearchButton);
                    console.log(e);
                }
            });
        }
    }

</script>
@endsection