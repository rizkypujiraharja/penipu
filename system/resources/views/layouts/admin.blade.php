@extends('layouts.app')
@section('main-menu')
              <ul>
                <li class="{{ route('home') == request()->url() ? 'active' : '' }}"><a href="{{ route('home') }}">Beranda</a></li>
                <li class="{{ route('page', 'syarat-ketentuan') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','syarat-ketentuan') }}">Syarat & Ketentuan</a></li>
                <li class="{{ route('page' ,'disclaimer') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','disclaimer') }}">Disclaimer</a></li>
                <li><a href="#">Lainnya <i class="fa fa-caret-down"></i></a>
                  <ul class="dropdown">
                    <li class="{{ route('page','about') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','about') }}"><i class="fa fa-angle-double-right"></i> Tentang Layanan</a></li>
                  </ul>
                </li>
                <li class="{{ route('account.newreport') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.newreport') }}">Kirim Laporan</a></li>
                <li class="btn_item">
                    <ul>
                        <li>
                            <button class="btn_login" onclick="javascript:window.open('{{ route('account.home') }}', '_self');">User Panel</button>
                        </li>
                    </ul>   
                </li>
              </ul>
@endsection
@section('content')
<div id="dashboard_inner_block">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="dashboard_nav_item hidden-xs hidden-sm">
              <ul>
                <li class="{{ route('account.home') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.home') }}"><i class="fa fa-user"></i> Member Area</a></li>
                <li class="{{ route('admin.home').'/posts' == request()->url() ? 'active' : '' }}"><a href="{{ route('admin.home') }}"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li class="{{ route('admin.verification') == request()->url() ? 'active' : '' }}"><a href="{{ route('admin.verification') }}"><i class="fa fa-get-pocket"></i> Verifikasi Member</a></li>
                <li class="{{ route('page.index') == request()->url() ? 'active' : '' }}"><a href="{{ route('page.index') }}"><i class="fa fa-file"></i> Pages</a></li>
                <li class="{{ route('comment.index') == request()->url() ? 'active' : '' }}"><a href="{{ route('comment.index') }}"><i class="fa fa-comments"></i> Comment</a></li>
                <li class="{{ route('setting') == request()->url() ? 'active' : '' }}"><a href="{{ route('setting') }}"><i class="fa fa-gear"></i> Setting</a></li>
                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Keluar</a></li>
              </ul>
            </div>
            <div id="vfx-search-box" class="hidden-md hidden-lg">
              <select id="select-menu" class="form-control">
                <option value="">-- MENU NAVIGASI --</option>
                <option value="{{ route('account.home') }}" {{ route('admin.home') == request()->url() ? 'selected' : '' }}> Dashboard </option>
            </select>
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
              @if(Session::has('alert-error'))
                <div class="alert alert-danger" role="alert"> {{ Session::get('alert-error') }} </div>
              @elseif(Session::has('alert-success'))
                <div class="alert alert-success" role="alert"> {{ Session::get('alert-success') }} </div>
              @endif
              @yield('admin-content', '')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection