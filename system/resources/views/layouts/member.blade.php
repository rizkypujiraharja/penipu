@extends('layouts.app')
@section('content')
<div id="dashboard_inner_block">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="dashboard_nav_item hidden-xs hidden-sm">
              <ul>
                @role('admin')
                <li class="{{ route('admin.home') == request()->url() ? 'active' : '' }}"><a href="{{ route('admin.home') }}"><i class="fa fa-user-secret"></i> Admin Panel</a></li>
                @endrole
                <li class="{{ route('account.home') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.home') }}"><i class="fa fa-tachometer"></i> Dashboard</a></li>
                <li class="{{ route('account.newreport') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.newreport') }}"><i class="fa fa-pencil"></i> Kirim Laporan</a></li>
                <li class="{{ route('account.profile') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.profile') }}"><i class="fa fa-user"></i> Edit Profil</a></li>
                <li class="{{ route('account.verification') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.verification') }}"><i class="fa fa-get-pocket"></i> Verifikasi Akun</a></li>
                <li class="{{ route('account.changepassword') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.changepassword') }}"><i class="fa fa-key"></i> Ubah Kata Sandi</a></li>
                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Keluar</a></li>
              </ul>
            </div>
            <div id="vfx-search-box" class="hidden-md hidden-lg">
              <select id="select-menu" class="form-control">
                <option value="">-- MENU NAVIGASI --</option>
                @role('admin')
                <option value="{{ route('admin.home') }}" {{ route('admin.home') == request()->url() ? 'selected' : '' }}> Admin Panel </option>
                @endrole
                <option value="{{ route('account.home') }}" {{ route('account.home') == request()->url() ? 'selected' : '' }}> Dashboard </option>
                <option value="{{ route('account.newreport') }}" {{ route('account.newreport') == request()->url() ? 'selected' : '' }}> Kirim Laporan </option>
                <option value="{{ route('account.profile') }}" {{ route('account.profile') == request()->url() ? 'selected' : '' }}> Edit Profil </option>
                <option value="{{ route('account.verification') }}" {{ route('account.verification') == request()->url() ? 'selected' : '' }}> Verifikasi Akun </option>
                <option value="{{ route('account.changepassword') }}" {{ route('account.changepassword') == request()->url() ? 'selected' : '' }}> Ubah Kata Sandi </option>
                <option value="{{ route('logout') }}"> Keluar </option>
            </select>
            </div>
          </div>
          <div class="col-md-9 col-sm-8 col-xs-12">
              @if(Session::has('alert-error'))
                <div class="alert alert-danger" role="alert"> {{ Session::get('alert-error') }} </div>
              @elseif(Session::has('alert-success'))
                <div class="alert alert-success" role="alert"> {{ Session::get('alert-success') }} </div>
              @endif
              @yield('member-content', '')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection