<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>
    @if(!empty($__env->yieldContent('title')))
        @yield('title') - {{ config('setting.site_name') }}
    @else
        {{ config('setting.site_tagline') }} - {{ config('setting.site_name') }}
    @endif
</title>

<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('/assets/images/favicon.png') }}">
<!-- Style Sheets -->
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/css/animate.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/css/stylesheet.css?rev='.time()) }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/css/responsive_style.css') }}" type="text/css">
<!-- Font Awesome Fonts-->
<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/font-awesome.min.css') }}">
<!-- Sweet Alert -->
<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/sweetalert2.min.css') }}">
<!-- Toastr -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<!-- Google Fonts-->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
<!-- Custom -->
<link rel="stylesheet" href="{{ asset('/assets/css/custom.css?rev='.time()) }}" type="text/css">

<style>
    ul.use-slt-link li {
        border-bottom: none !important;
    }
    .vfx-item-counter-up .count_number {
        font-size: 35px !important;
    }
</style>
@yield('css', '')

</head>

<body>
<div id="vfx_loader_block">
  <div class="vfx-loader-item"> <img src="{{ asset('/assets/images/loading.gif') }}" alt="" /> </div>
</div>
<div id="logo-header" data-spy="affix" data-offset-top="500">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-xs-9">
        <div id="logo"> <a href="{{ route('home') }}"><img src="{{ asset('/assets/images/logo-200.png') }}" alt="logo"></a> </div>
      </div>
      <div class="col-sm-9 text-right">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#thrift-1" aria-expanded="false"> <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="collapse navbar-collapse" id="thrift-1"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
            <div id="nav_menu_list">
              @if(View::hasSection('main-menu'))
                @yield('main-menu')
              @else
              <ul>
                <li class="{{ route('home') == request()->url() ? 'active' : '' }}"><a href="{{ route('home') }}">Beranda</a></li>
                <li class="{{ route('page', 'syarat-ketentuan') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','syarat-ketentuan') }}">Syarat & Ketentuan</a></li>
                <li class="{{ route('page' ,'disclaimer') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','disclaimer') }}">Disclaimer</a></li>
                <li><a href="#">Lainnya <i class="fa fa-caret-down"></i></a>
                  <ul class="dropdown">
                    <li class="{{ route('page','about') == request()->url() ? 'active' : '' }}"><a href="{{ route('page','about') }}"><i class="fa fa-angle-double-right"></i> Tentang Layanan</a></li>
                  </ul>
                </li>
                <li class="{{ route('account.newreport') == request()->url() ? 'active' : '' }}"><a href="{{ route('account.newreport') }}">Kirim Laporan</a></li>
                <li class="btn_item">
                    <ul>
                        @if(Auth::check())
                            <li>
                              <button class="btn_login" onclick="javascript:window.open('{{ route('account.home') }}', '_self');">Akun Saya</button>
                            </li>
                        @else
                            <li>
                              <button class="btn_login" data-toggle="modal" data-target="#login">Masuk</button>
                            </li>
                            <li>
                              <button class="btn_register" data-toggle="modal" data-target="#register">Mendaftar</button>
                            </li>
                        @endif
                    </ul>   
                </li>
              </ul>
              @endif
            </div>
          </div>
        </nav>
      </div>
    </div>
  </div>
</div>

@yield('breadcrumb', '')

@yield('content', '')

<footer class="site-footer footer-map">
  <div class="footer-top" style="padding-bottom:35px;">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2 style="text-transform: uppercase;">{{ config('setting.site_name') }}</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Privacy & Policy</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Payment Method</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Sitemap</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Support</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Terms & Condition</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>HALAMAN</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Privacy & Policy</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Payment Method</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Sitemap</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Support</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Terms & Condition</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>BANTUAN</h2>
          <hr>
          <ul class="use-slt-link">
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Privacy & Policy</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Payment Method</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Sitemap</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Support</a></li>
            <li><a href="#"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Terms & Condition</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <h2>Have you any Query?</h2>
          <hr>
          
        </div>
      </div>
      <div class="row">
    <div class="col-12" style="border-top: 1px solid #353535;padding: 30px 0px 0px 0px;text-align: center;font-size: 13px;margin-top: 65px;">
        
       {{ config('setting.site_name') }} merupakan database arsip laporan penipuan online yang bertujuan untuk membantu pengguna memeriksa kredibilitas pihak tertentu dan mencegah adanya tindakan penipuan online. Arsip-arsip yang ada di dalam situs ini adalah laporan yang dikirim oleh para pengguna dan merupakan tanggung jawab masing-masing pengirim. Meskipun telah melalui tahap review sebelum ditampilkan, akan tetapi TIDAK ADA jaminan bahwa data tersebut 100% akurat. Semua keputusan adalah independen dan kami tidak pernah menyarankan maupun mencegah pengguna untuk bertransaksi. Pengguna disarankan membaca Persyaratan Layanan dan Disclaimer dengan seksama.

       <ul class="social-icons" style="margin-top:20px">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
          </ul>
        
    </div>
</div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <p class="text-xs-center">Hak Cipta &copy; {{ date("Y") }} {{ config('setting.site_name') }}. Semua hak dilindungi. Dikembangkan oleh <a target="_blank" href="//www.zeros.co.id" style="display:inline !important">Zeros Dev</a> untuk <a target="_blank" href="//tridi.net" style="display:inline !important">PT Trijaya Digital Grup</a></p>
        </div>
        <div><a href="#" class="scrollup">Scroll</a></div>
      </div>
    </div>
  </div>
</footer>
<!--================================ Login and Register Forms ===========================================--> 

<!-- login form -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="listing-modal-1 modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"> Masuk ke Akun</h4>
      </div>
      <div class="modal-body">
        <div class="listing-login-form">
          <form action="{{ url('/auth/login') }}" method="POST">
            @csrf
            <div class="listing-form-field"> <i class="fa fa-envelope blue-1"></i>
              <input class="form-field bgwhite" type="email" name="email" placeholder="Email" />
            </div>
            <div class="listing-form-field"> <i class="fa fa-lock blue-1"></i>
              <input class="form-field bgwhite" type="password" name="password" placeholder="Kata Sandi"  />
            </div>
            <div class="listing-form-field clearfix margin-top-20 margin-bottom-20">
              <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" name="remember" />
              <label for="checkbox-1-1"></label>
              <label class="checkbox-lable">Ingat Saya</label>
              <a href="#" onclick="changeAuthForm('reset');">Lupa kata sandi?</a> </div>
            <div class="listing-form-field">
              <input class="submit" type="submit" value="login" />
            </div>
          </form>
          <div class="bottom-links">
            <p>Belum Memiliki akun ?<a href="#" onclick="changeAuthForm('register');">Buat akun</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- registration form -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="listing-modal-1 modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel2">Pendaftaran</h4>
      </div>
      <div class="modal-body">
        <div class="listing-register-form">
          <form action="{{ url('/auth/register') }}" method="POST">
            @csrf
            <div class="listing-form-field"> <i class="fa fa-user blue-1"></i>
              <input class="form-field bgwhite" type="text" name="name" placeholder="Nama lengkap sesuai identitas"  />
            </div>
            <div class="listing-form-field"> <i class="fa fa-envelope blue-1"></i>
              <input class="form-field bgwhite" type="email" name="email" placeholder="Alamat email" />
            </div>
            <div class="listing-form-field"> <i class="fa fa-lock blue-1"></i>
              <input class="form-field bgwhite" type="password" name="password" placeholder="Kata sandi"  />
            </div>
            <div class="listing-form-field"> <i class="fa fa-lock blue-1"></i>
              <input class="form-field bgwhite" type="password" name="password_confirmation" placeholder="Ulangi kata sandi" />
            </div>
            <div class="listing-form-field clearfix margin-top-20 margin-bottom-20 login_form_text_center">
              <input type="checkbox" id="checkbox-1-2" class="regular-checkbox" name="accept_terms" />
              <label for="checkbox-1-2"></label>
              <label class="checkbox-lable">saya menyetujui</label> &nbsp; <a target="_blank" href="{{ url('/pages/terms') }}">Syarat dan Ketentuan</a> </div>
            <div class="listing-form-field">
              <input class="submit" type="submit" value="Mendaftar" />
            </div>
          </form>
          <div class="bottom-links">
            <p>Sudah Memiliki akun ?<a href="#" onclick="changeAuthForm('login');">Masuk ke akun</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Reset Form -->
<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="listing-modal-1 modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"> Reset Kata Sandi</h4>
      </div>
      <div class="modal-body">
        <div class="listing-login-form">
          <form action="{{ url('/auth/password/email') }}" method="POST">
            @csrf
            <div class="listing-form-field clearfix margin-top-20 margin-bottom-20"> <i class="fa fa-envelope blue-1"></i>
              <input class="form-field bgwhite" type="email" name="email" placeholder="Email" />
            </div>
            <div class="listing-form-field">
              <input class="submit" type="submit" value="Lanjutkan" />
            </div>
          </form>
          <div class="bottom-links">
            <p>Belum Memiliki akun ?<a href="#" onclick="changeAuthForm('register');">Buat akun</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Scripts --> 
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script> 
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('/assets/js/waypoints.js') }}"></script> 
<script src="{{ asset('/assets/js/jquery_counterup.js') }}"></script> 
<script src="{{ asset('/assets/js/jquery_custom.js') }}"></script>
<script src="{{ asset('/assets/js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/assets/js/homemap_custom.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>

    $("body").on("click", ".spinning-onclick", function() {
      $(this).html('<i class="fa fa-spinner fa-md spinning"></i>');
    });

    $("#select-menu").on("change", function() {
      var url = $("option:selected", this).val();
      if( url.length > 0 && url != '' ) {
        window.location.assign(url);
      }
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $(document).ready(function() {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "2000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };

      @if(Session::has('flash-error'))
        toastr['error']('{{ Session::get('flash-error') }}');
      @elseif(Session::has('flash-success'))
        toastr['success']('{{ Session::get('flash-success') }}');
      @elseif(Session::has('flash-warning'))
        toastr['warning']('{{ Session::get('flash-warning') }}');
      @elseif(Session::has('flash-info'))
        toastr['info']('{{ Session::get('flash-info') }}');
      @endif

  });

  function hideAllAuthForm() {
    $("#reset").modal('hide');
    $("#login").modal('hide');
    $("#register").modal('hide');
  }

  function changeAuthForm(id) {
    if( id == 'register' ) {
      hideAllAuthForm();
      $("#register").modal('show');
    }
    else if( id == 'login' ) {
      hideAllAuthForm();
      $("#login").modal('show');
    }
    else if( id == 'reset' ) {
      hideAllAuthForm();
      $("#reset").modal('show');
    }
  }

</script>

@yield('js', '')

</body>
</html>