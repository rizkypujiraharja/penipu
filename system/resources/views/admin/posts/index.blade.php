@extends('layouts.admin')
@section('title', 'Laporan | Admin')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('admin-content')
<div class="">
	<table class="table table-hover table-stripped table-bordered">
		<thead>
			<tr style="background: #0CB4FF;color:#fff">
				<th style="width:90px">ID</th>
				<th>Preview Data</th>
				<th>Dibuat</th>
				<th class=" text-center" style="width:120px">Status</th>
				<th class=" text-center">Tindakan</th>
			</tr>
		</thead>
		<tbody id="report-table-body">
			<tr>
				<td><input type="number" id="id" class="form-control" name="id" placeholder="" value="" style="box-shadow: none;"></td>
				<td><input type="text" id="preview_data" class="form-control" name="preview_data" placeholder="" value="" style="box-shadow: none;"></td>
				<td><input type="text" id="date" class="form-control" name="date" placeholder="" value="" style="box-shadow: none;"></td>
				<td>
					<select id="status" class="form-control" name="status" style="box-shadow: none;">
						@php
							$_p = new App\Models\Post;
						@endphp
                  		<option value="all">Semua</option>
                  		<option value="{{ $_p->getConstant('STATUS_DRAFT') }}">Konsep</option>
                  		<option value="{{ $_p->getConstant('STATUS_REVIEW') }}">Review</option>
                  		<option value="{{ $_p->getConstant('STATUS_PUBLIC') }}">Dipublish</option>
                  		<option value="{{ $_p->getConstant('STATUS_HOLD') }}">Ditahan</option>
                  		<option value="{{ $_p->getConstant('STATUS_REJECTED') }}">Ditolak</option>
                	</select>
                </td>
				<td>
					<button id="search-button" onclick="searchData();" class="btn btn-default" style="background: #0CB4FF;color:#fff">Cari</button>
				</td>
			</tr>
			@if(count($posts) > 0)
				@foreach($posts as $post)
				<tr class="data-child">
					<td class="text-center"><b>{{ $post->id }}</b></td>
					<td style="white-space: normal; word-wrap: break-word;">
						<b>Nama :</b> {{ json_decode($post->nama)[0] }}
						<br/>
						@if($post->rekening_bank != '[]')
							@php
								$b = json_decode($post->rekening_bank);
							@endphp
							<b>Rekening Bank :</b> {{ $b[0]->nama_bank }} {{ $b[0]->nomor_rekening }} A/N {{ $b[0]->nama_pemilik }}
						@endif
					</td>
					<td style="white-space: normal; word-wrap: break-word;">{{ userDate($post->created_at, 'd-m-Y H:i', $signed_user->timezone) }}</td>
					<td class=" text-center">
						@switch($post->status)
							@case($post->getConstant('STATUS_DRAFT'))
								<span class="label label-default">KONSEP</span>
								@break;

							@case($post->getConstant('STATUS_REVIEW'))
								<span class="label label-primary">REVIEW</span>
								@break;

							@case($post->getConstant('STATUS_PUBLIC'))
								<span class="label label-success">DIPUBLISH</span>
								@break;

							@case($post->getConstant('STATUS_HOLD'))
								<span class="label label-warning">DITAHAN</span>
								@break;

							@case($post->getConstant('STATUS_REJECTED'))
								<span class="label label-danger">DITOLAK</span>
								@break;

							@default
								@break
						@endswitch
					</td>
					<td class=" text-center">
						<div class="dropdown">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{ $post->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="background: #0CB4FF;color:#fff">
						    Tindakan
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{ $post->id }}">
						    <li><a target="_blank" href="{{ route('admin.posts.edit', ['id' => $post->id]) }}"><i class="fa fa-pencil"></i> Edit</a></li>
						    @if($post->status == $post->getConstant('STATUS_PUBLIC'))
						    	<li><a target="_blank" href="{{ route('post.view', ['id' => $post->id]) }}"><i class="fa fa-eye"></i> Lihat</a></li>
						    @else
						    	<li><a target="_blank" href="{{ route('admin.posts.preview', ['id' => $post->id]) }}"><i class="fa fa-eye"></i> Pratinjau</a></li>
						    @endif
						    <li role="separator" class="divider"></li>
						    <li><a class="delete-post" data-post-id="{{ $post->id }}" href="#"><i class="fa fa-trash"></i> Hapus</a></li>
						  </ul>
						</div>
					</td>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">Belum ada laporan</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="vfx-person-block">
          <center>
            Page {{ $page }}
          </center>
            <ul class="vfx-pagination">
              <li><a href="{{$request->fullUrlWithQuery(['page' => 1])}}"><i class="fa fa-angle-double-left"></i></a></li>
              @php
                if ($page <= 3 || $jumpage < $page) {
                  $start = 1;
                  if($jumpage < 5){
                  	$end = $jumpage;
                  }else{
                  	$end = 5;
                  }
                } else if ($jumpage - 2 < $page ) {
                  $start = $jumpage - 4;
                  $end = $jumpage;
                }else{
                  $start = $page - 2;
                  $end = $start + 4;
                }
              @endphp
            @for ($i = $start; $i <= $end; $i++)
              <li class="{{$i == $page ? 'active' : ''}}"><a href="{{$request->fullUrlWithQuery(['page'=>$i])}}">{{$i}}</a></li>
            @endfor
              <li><a href="{{$request->fullUrlWithQuery(['page' => $jumpage])}}"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
</div>
@endsection

@section('js')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

	$(function() {
		$("#date").datepicker({dateFormat: 'dd-mm-yy'});
	});

	function createDate(date) {
	    var d = new Date(date);
	    var timestamp = (d.getTime() / 1000);
	    var WIB = timestamp + 25200;
	    	WIB = WIB * 1000;

	    var dx = new Date(WIB);
	   
	    var hh = addZero(dx.getHours());
	    var mm = addZero(dx.getMinutes());
	    var ss = addZero(dx.getSeconds());
	    var EE = dx.getDay();
	    var dd = addZero(dx.getDate());       //day
	    var MM = addZero(dx.getMonth() + 1); //month
	    var yy = dx.getFullYear();   //year

	    function addZero(i) {
	        if (i < 10) {
	            i = "0" + i;
	        }
	        return i;
	    }
	    
	    return dd+"-"+MM+"-"+yy+" "+hh+":"+mm;
	}

	function searchData() {
		var id = $("#id").val();
		var preview = $("#preview_data").val();
		var date = $("#date").val();
		var status = $("#status option:selected").val();

		$("#search-button").html('<i class="fa fa-spinner fa-lg spinning"></i>');

		var url = "{{ route('admin.posts.index') }}?search=1&id=" + id + "&preview=" + preview + "&date=" + date + "&status=" + status + "&offset=0&limit=10";

		window.open(url, '_self');

		return true;
	}

	$("body").on("click", "a.delete-post", function() {
		var post_id = parseInt($(this).data("post-id"));
        swal({
          title: "Anda yakin akan menghapus laporan #"+ post_id +" ?",
          text: "Setelah dihapus laporan tidak dapat dikembalikan",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location.assign('{{ url('/admin/posts/delete') }}/' + post_id);
          }
        });
    });
</script>
@endsection

