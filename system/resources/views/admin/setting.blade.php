@extends('layouts.member')
@section('title', 'Edit Profile')

@section('css')
<style type="text/css">
.profile-label {padding:5px 15px;float:left}
</style>
@endsection

@section('member-content')
<div class="submit_listing_box">
    <h3>Website Setting</h3>
    <form class="form-alt" action="" method="POST">
    	@csrf
        <div class="row">
            <form action="{{ route('account.profile.store') }}" class="form-control">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Judul Situs</label>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="" required>
                            @if ($errors->has('name'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="profile-label">Deskripsi Singkat</label>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="" required>
                            @if ($errors->has('email'))
                            <div class="form-error-message">
                                <i class="glyphicon glyphicon-remove-circle"></i> 
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12 pull-left">
                    <button class="btn btn-primary pull-left" type="submit">Simpan</button>
                </div>
            </form>
        </div>
    </form>
</div>
@endsection