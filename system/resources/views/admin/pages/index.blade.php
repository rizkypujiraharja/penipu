@extends('layouts.admin')
@section('title', 'Halaman | Admin')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('admin-content')
<div class="">
	<a href="{{ route('page.create') }}" class="btn btn-primary">Tambah Halaman</a>
	<table class="table table-hover table-stripped table-bordered" style="margin-top: 7px">
		<thead>
			<tr style="background: #0CB4FF;color:#fff">
				<th style="width:90px">ID</th>
				<th>Title</th>
				<th class="text-center" style="width:90px">Status</th>
				<th class="text-center" style="width:120px">Tindakan</th>
			</tr>
		</thead>
		<tbody id="report-table-body">
			@if(count($pages) > 0)
				@foreach($pages as $page)
				<tr class="data-child">
					<td class="text-center"><b>{{ $page->id }}</b></td>
					<td>
						{{ $page->title }}
					</td>
					<td class=" text-center">
						@switch($page->status)
							@case(1)
								<span class="label label-success">Active</span>
								@break;
							@case(0)
								<span class="label label-danger">Nonactive</span>
								@break;
							@default
								@break
						@endswitch
					</td>
					<td class=" text-center">
						<a href="{{ route('page.show', $page) }}">
						  <span class="label label-info"><i class="fa fa-eye"></i></span>
						</a>
						<a href="{{ route('page.edit', $page) }}">
						  <span class="label label-warning"><i class="fa fa-pencil"></i></span>
						</a>
						<button class="btn btn-xs btn-danger" href="{{ route('page.destroy', $page) }}" id="delete" data-title="{{$page->title}}">
						  <i class="fa fa-trash-o"></i>
						</button>
					</td>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">Belum ada data</td>
				</tr>
			@endif
		</tbody>
	</table>
</div>

                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <input type="submit" style="display: none;">
                </form>
@endsection

@section('js')
<script type="text/javascript">
    $('button#delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');
        swal({
          title: "Anda yakin akan menghapus halaman "+ title +" ?",
          text: "Setelah dihapus halaman tidak dapat dikembalikan",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        });
    });
</script>
@endsection