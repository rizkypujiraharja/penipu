@extends('layouts.admin')
@section('title', 'Edit Halaman')
@section('css')
<style type="text/css">
	.screenshot { padding: 10px; max-height: 150px }
	.img-ss { min-height: 180px; }
</style>
@endsection
@section('admin-content')
<form action="{{ route('page.update' ,$page) }}" method="POST" enctype="multipart/form-data" id="new-post-form">
	@csrf
  @method('PATCH')
            <div class="submit_listing_box">
              <h3>Edit Halaman</h3>
              <div class="form-alt" style="margin-bottom: 20px">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label style="font-size:16px;font-weight:500">Judul Halaman :*</label>
                    <input placeholder="(wajib)" class="form-control" type="text" value="{{ old('title') ?? $page->title}}" name="title" required="">
                    @if ($errors->has('title'))
						<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('title') }}</div>
					@endif
                  </div>
                </div>
              </div>
              <div class="form-alt">
              	<div class="row">
              		<div class="col-md-12 col-sm-12 col-xs-12"><label style="font-size:16px;font-weight:500">Konten (min. 300 karakter) :*</label></div>
	                <div class="form-group col-md-12 col-sm-12 col-xs-12">
	                  	<textarea placeholder="(wajib)" class="form-control" rows="5" name="content">{!! old('content')  ?? $page->content !!}</textarea>
	                  	@if ($errors->has('content'))
						<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('content') }}</div>
					@endif
	                </div>
	                <div class="clearfix"></div>
	            </div>
              </div>
              <div class="form-alt" style="margin-bottom: 20px">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label style="font-size:16px;font-weight:500">Status :*</label>
                    <select name="status" class="form-control" required="">
                    	<option value="1">Aktif</option>
                    	<option value="0" {{ $page->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                    </select>
                    @if ($errors->has('status'))
						<div class="form-error-message"><i class="glyphicon glyphicon-remove-circle"></i> {{ $errors->first('status') }}</div>
					@endif
                  </div>
                </div>
              </div>
                <div class="from-list-lt">
	              <div class="form-group">
	                <button class="btn" type="submit">Simpan</button>
	              </div>
	            </div>
            </div>
</form>
@endsection

@section('js')
<script src="{{ asset('/assets/vendor/wysiwyg/js/tinymce.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
      tinymce.init({
          selector: 'textarea[name=content]',
          height: 200,
          theme: 'modern',
          plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
          ],
          toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
          image_advtab: true
      });
 });
</script>
@endsection