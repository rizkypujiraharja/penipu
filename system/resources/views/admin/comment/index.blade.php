@extends('layouts.admin')
@section('title', 'Komentar | Admin')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('admin-content')
<div class="">
	<table class="table table-hover table-stripped table-bordered" style="margin-top: 7px">
		<thead>
			<tr style="background: #0CB4FF;color:#fff">
				<th style="width:90px">ID POST</th>
				<th>User & Komentar</th>
				<th class="text-center" style="max-width:120px">Status</th>
				<th class="text-center">Tindakan</th>
			</tr>
		</thead>
		<tbody id="report-table-body">
			<tr>
			<form action="" method="get">
				<td><input type="number" class="form-control" name="id_post" placeholder="" value="{{$request->id_post}}" style="box-shadow: none;"></td>
				<td><input type="text" name="nama" placeholder="Cari berdasarkan nama" class="form-control" placeholder="" value="{{$request->nama}}" style="box-shadow: none;"></td>
				<td>
					<select id="deleted" class="form-control" name="deleted" style="box-shadow: none;">
                  		<option value="all" {{$request->deleted === Null ? 'selected' : ''}}>Semua</option>
                  		<option value="1" {{ $request->deleted == 1 ? 'selected' : ''}}>Deleted</option>
                  		<option value="0" {{ $request->deleted == '0' ? 'selected' : ''}}>Available</option>
                	</select>
                </td>
				<td class="text-center">
					<button id="search-button" onclick="searchData();" class="btn btn-default" style="background: #0CB4FF;color:#fff">Cari</button>
				</td>
			</form>
			</tr>
			@if(count($comments) > 0)
				@foreach($comments as $comment)
				<tr class="data-child">
					<td class="text-center"><b>{{ $comment->post_id }}</b></td>
					<td>
						<b>{{ $comment->name }}</b><br>
						{!! html_entity_decode($comment->content) !!}
					</td>
					<td class=" text-center">
						@if($comment->deleted == 1)
							<span class="label label-danger">Deleted</span>
						@else
							<span class="label label-success">Available</span>
						@endif
					</td>
					<td class=" text-center">
						<a href="{{ route('post.view', $comment->post_id) }}" target="_blank">
						  <span class="label label-info">Lihat</span>
						</a>
						@if ($comment->deleted == 0)
						<button class="btn btn-xs btn-danger" href="{{ route('comment.delete', $comment) }}" id="delete" data-title="{{$comment->name}}" data-child="{{$comment->content}}">
						  Hapus
						</button>
						@endif
					</td>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">Belum ada data</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="vfx-person-block">
          <center>
            Page {{ $page }}
          </center>
            <ul class="vfx-pagination">
              <li><a href="{{$request->fullUrlWithQuery(['page' => 1])}}"><i class="fa fa-angle-double-left"></i></a></li>
              @php
                if ($page <= 3 || $jumpage < $page) {
                  $start = 1;
                  if($jumpage < 5){
                  	$end = $jumpage;
                  }else{
                  	$end = 5;
                  }
                } else if ($jumpage - 2 < $page ) {
                  $start = $jumpage - 4;
                  $end = $jumpage;
                }else{
                  $start = $page - 2;
                  $end = $start + 4;
                }
              @endphp
            @for ($i = $start; $i <= $end; $i++)
              <li class="{{$i == $page ? 'active' : ''}}"><a href="{{$request->fullUrlWithQuery(['page'=>$i])}}">{{$i}}</a></li>
            @endfor
              <li><a href="{{$request->fullUrlWithQuery(['page' => $jumpage])}}"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
</div>

				<form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <input type="submit" style="display: none;">
                </form>
@endsection

@section('js')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $('button#delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');
        var coment = $(this).data('child');
        swal({
          title: "Anda yakin akan menghapus komentar "+ title +" ?",
          text: coment,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        });
    });
</script>
@endsection