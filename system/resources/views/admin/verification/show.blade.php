@extends('layouts.admin')
@section('title', 'Verifikasi User | Admin')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('admin-content')
<div class="">
	<table class="table">
		<tr>
			<td>ID</td>
			<td>{{$userverif->__user->id}}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>{{$userverif->__user->name}}</td>
		</tr>
		<tr>
			<td>Email</td>
			<td>{{$userverif->__user->email}}</td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td>{{$userverif->__user->alamat}}</td>
		</tr>
		<tr>
			<td>Kota</td>
			<td>{{$userverif->__user->kota}}</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>{{$userverif->__user->provinsi}}</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>{{$userverif->__user->kode_pos}}</td>
		</tr>
		@if ($userverif->status == 1)
		<tr>
			<td>Info</td>
			<td>{{$userverif->additional_info}}</td>
		</tr>
		@endif
	</table>
	
	@if ($userverif->status == 0)
	<form action="{{ route('admin.verification.action', $userverif->user_id) }}" method="POST">
	@csrf
	<div class="row" style="padding-bottom: 20px">
		<div class="col-md-2">Info</div>
		<div class="col-md-10">
			<input type="text" name="reason" class="form-control" required="">
		</div>
	</div>
	<div class="row" style="padding-bottom: 20px">
		<div class="col-md-6">
			<input type="submit" name="action" value="Approve" class="btn btn-success btn-block">
		</div>
		<div class="col-md-6">
			<input type="submit" name="action" value="Non Approve" class="btn btn-danger btn-block">
		</div>
	</div>
	</form>
	@endif
	
	<div class="row">
		<div class="col-md-6 text-center">
			<div class="panel panel-info">
			  <div class="panel-heading">Photo :
				<a href="{{url('/assets/images/user-verification/'.$userverif->photo)}}" target="_blank">
					<span class="label label-primary">Lihat</span>
				</a>
			  </div>
			  <div class="panel-body">
				<img src="{{url('/assets/images/user-verification/'.$userverif->photo)}}">
			  </div>
			</div>
		</div>
		<div class="col-md-6 text-center">
			<div class="panel panel-info">
			  <div class="panel-heading">Identitas :
				<a href="{{url('/assets/images/user-verification/'.$userverif->id_document)}}" target="_blank">
					<span class="label label-primary">Lihat</span>
				</a>
			  </div>
			  <div class="panel-body">
				<img src="{{url('/assets/images/user-verification/'.$userverif->id_document)}}">
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection
