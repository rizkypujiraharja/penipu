@extends('layouts.admin')
@section('title', 'Verifikasi User | Admin')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('admin-content')
<div class="">
	
                    @if(Session::has('alert-success'))
                        <div class="alert alert-success">
                            <p>{{ Session::get('alert-success') }}</p>
                        </div>
                    @endif
                    @if(Session::has('alert-error'))
                        <div class="alert alert-danger">
                            <p>{{ Session::get('alert-error') }}</p>
                        </div>
                    @endif
	<table class="table table-hover table-stripped table-bordered" style="margin-top: 7px">
		<thead>
			<tr style="background: #0CB4FF;color:#fff">
				<th style="width:90px">ID User</th>
				<th>User</th>
				<th>Tanggal Verifikasi</th>
				<th class="text-center" style="max-width:120px">Status</th>
				<th class="text-center">Tindakan</th>
			</tr>
		</thead>
		<tbody id="report-table-body">
			<tr>
			<form action="" method="get">
				<td><input type="number" class="form-control" name="id" placeholder="" value="{{$request->id}}" style="box-shadow: none;"></td>
				<td><input type="text" name="nama" class="form-control" placeholder="" value="{{$request->nama}}" style="box-shadow: none;"></td>
				<td><input type="text" id="date" class="form-control" name="date" placeholder="" value="" style="box-shadow: none;"></td>
				<td>
					<select id="status" class="form-control" name="status" style="box-shadow: none;">
						@php
							$_p = new App\Models\UserVerification;
						@endphp
                  		<option value="all" {{$request->status === Null ? 'selected' : ''}}>Semua</option>
                  		<option value="{{ $_p->getConstant('STATUS_REVIEW') }}" {{$_p->getConstant('STATUS_REVIEW') === $request->status ? 'selected' : ''}}>Review</option>
                  		<option value="{{ $_p->getConstant('STATUS_APPROVED') }}" {{$_p->getConstant('STATUS_APPROVED') === $request->status ? 'selected' : ''}}>Approved</option>
                	</select>
                </td>
				<td class="text-center">
					<button id="search-button" onclick="searchData();" class="btn btn-default" style="background: #0CB4FF;color:#fff">Cari</button>
				</td>
			</form>
			</tr>
			@if(count($verifications) > 0)
				@foreach($verifications as $verif)
				<tr class="data-child">
					<td class="text-center"><b>{{ $verif->id }}</b></td>
					<td>
						{{ $verif->name }}
					</td>
					<td>
						{{ userDate($verif->created_at, 'd-m-Y H:i', $signed_user->timezone) }}
					</td>
					<td class=" text-center">
						@switch($verif->status)
							@case($verif->getConstant('STATUS_REVIEW'))
								<span class="label label-primary">REVIEW</span>
								@break;
							@case($verif->getConstant('STATUS_APPROVED'))
								<span class="label label-success">Approved</span>
								@break;

							@default
								@break
						@endswitch
					</td>
					<td class=" text-center">
						<div class="dropdown">
						<a href="{{ route('admin.verification.show', $verif->id) }}">
						  <span class="label label-info">Lihat</span>
						</a>
						</div>
					</td>
				</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">Belum ada data</td>
				</tr>
			@endif
		</tbody>
	</table>
	<div class="vfx-person-block">
          <center>
            Page {{ $page }}
          </center>
            <ul class="vfx-pagination">
              <li><a href="{{$request->fullUrlWithQuery(['page' => 1])}}"><i class="fa fa-angle-double-left"></i></a></li>
              @php
                if ($page <= 3 || $jumpage < $page) {
                  $start = 1;
                  if($jumpage < 5){
                  	$end = $jumpage;
                  }else{
                  	$end = 5;
                  }
                } else if ($jumpage - 2 < $page ) {
                  $start = $jumpage - 4;
                  $end = $jumpage;
                }else{
                  $start = $page - 2;
                  $end = $start + 4;
                }
              @endphp
            @for ($i = $start; $i <= $end; $i++)
              <li class="{{$i == $page ? 'active' : ''}}"><a href="{{$request->fullUrlWithQuery(['page'=>$i])}}">{{$i}}</a></li>
            @endfor
              <li><a href="{{$request->fullUrlWithQuery(['page' => $jumpage])}}"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
        </div>
</div>
@endsection

@section('js')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection