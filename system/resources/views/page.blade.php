@extends('layouts.app')
@section('title', $page->title)
@section('css')
<link rel="stylesheet" href="{{ asset('/assets/css/superlist.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/js/owl.carousel/owl.carousel.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/js/colorbox/example1/colorbox.css') }}" type="text/css">
@endsection
@section('content')
<div class="details-lt-block">
  <div class="container header_slt_block">
    <div class="slt_item_head">
    </div>
  </div>
</div>
<div id="vfx-product-inner-item">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-8 col-xs-12">
        <h3>{{$page->title}}</h3>
        {!! html_entity_decode($page->content) !!}
      </div>
      <div class="col-md-3 col-sm-4 col-xs-12">
          
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('/assets/js/colorbox/jquery.colorbox-min.js') }}"></script> 
<script src="{{ asset('/assets/js/bootstrap-select/bootstrap-select.min.js') }}"></script> 
<script src="{{ asset('/assets/js/owl.carousel/owl.carousel.js') }}"></script> 
<script src="{{ asset('/assets/js/superlist.js') }}"></script>
@endsection