@extends('layouts.app')
@section('title', $nama[0])
@section('css')
<link rel="stylesheet" href="{{ asset('/assets/css/superlist.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/js/owl.carousel/owl.carousel.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('/assets/js/colorbox/example1/colorbox.css') }}" type="text/css">
@endsection
@section('content')
<div class="details-lt-block">
  <div class="container header_slt_block">
    <div class="slt_item_head">
    </div>
  </div>
</div>
<div id="vfx-product-inner-item">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-sm-8 col-xs-12">
        <div class="slider">
          <div class="detail-gallery">
            <div class="detail-gallery-preview"> <a href="{{ asset('/assets/images/post/'.$screenshot[0]) }}"> <img src="{{ asset('/assets/images/post/'.$screenshot[0]) }}" alt="" /> </a> </div>
            <ul class="detail-gallery-index">
              @foreach($screenshot as $ss)
                <li class="detail-gallery-list-item active"><a data-target="{{ asset('/assets/images/post/'.$ss) }}"><img src="{{ asset('/assets/images/post/'.$ss) }}" alt=""> </a> </li>
              @endforeach
            </ul>
          </div>
        </div>

        <div class="dlt-title-item">
          <h2>DATA TERLAPOR</h2>
          <p>
            <div class="table-responsive" style="min-width: -webkit-fill-available;">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr style="background:#ffce10">
                    <th>Kategori</th>
                    <th>Data</th>
                  </tr>
                </thead>
                <tbody>

                  @if(count($nama) > 0)
                    @php $namai = 0; @endphp
                    @foreach($nama as $n)
                      @php $namai++; @endphp
                      @if($namai == 1)
                        <tr>
                          <td rowspan="{{ count($nama) }}"><b>Nama</b></td>
                          <td>{{ $n }}</td>
                        </tr>
                      @else
                        <tr>
                          <td>{{ $n }}</td>
                        </tr>
                      @endif
                    @endforeach
                  @endif

                  @if(count($rekening_bank) > 0)
                    @php $reki = 0; @endphp
                    @foreach($rekening_bank as $rek)
                      @php $reki++; @endphp
                      @if($reki == 1)
                        <tr>
                          <td rowspan="{{ count($rekening_bank) }}"><b>Rekening Bank</b></td>
                          <td style="white-space: normal; word-wrap: break-word;">Bank : {{ $rek->nama_bank }}<br/>{{ $rek->nomor_rekening }} A/N {{ $rek->nama_pemilik }}</td>
                        </tr>
                      @else
                        <tr>
                          <td style="white-space: normal; word-wrap: break-word;">Bank : {{ $rek->nama_bank }}<br/>{{ $rek->nomor_rekening }} A/N {{ $rek->nama_pemilik }}</td>
                        </tr>
                      @endif
                    @endforeach
                  @endif

                  @if(!empty($post->alamat))
                    <tr>
                      <td rowspan="1"><b>Alamat</b></td>
                      <td style="white-space: normal; word-wrap: break-word;">{!! html_entity_decode($post->alamat, ENT_QUOTES) !!}</td>
                    </tr>
                  @endif

                  @if(count($nomor_telepon) > 0)
                    <tr>
                      <td><b>Nomor Telepon</b></td>
                      <td style="white-space: normal; word-wrap: break-word;">{{ implode(', ', $nomor_telepon) }}</td>
                    </tr>
                  @endif

                  @if(count($email) > 0)
                    <tr>
                      <td><b>Email</b></td>
                      <td style="white-space: normal; word-wrap: break-word;">{{ implode(', ', $email) }}</td>
                    </tr>
                  @endif

                  @if(count($media_sosial) > 0)
                    @php $msi = 0; @endphp
                    @foreach($media_sosial as $ms)
                      @php $msi++; @endphp
                      @if($msi == 1)
                        <tr>
                          <td rowspan="{{ count($media_sosial) }}"><b>Akun Media Sosial</b></td>
                          <td><a target="_blank" rel="nofollow, noopener" href="{{ $ms }}">{{ $ms }}</a></td>
                        </tr>
                      @else
                        <tr>
                          <td><a target="_blank" rel="nofollow, noopener" href="{{ $ms }}">{{ $ms }}</a></td>
                        </tr>
                      @endif
                    @endforeach
                  @endif

                </tbody>
              </table>
            </div>
          </p>
        </div>

        <div class="dlt-title-item">
          <h2>KRONOLOGI</h2>
          <p>{!! html_entity_decode($post->kronologi, ENT_QUOTES) !!}</p>
        </div>

        <div class="dlt-com-lt-block">
            <div class="dlt-com-lt-text" style="padding-left:0px">
              <div class="dlt-com-lt-title">Disclaimer!</div>
              <p>
                <ol>
                  <li>Konten dalam postingan ini dikirimkan oleh <b>{{ $post->anonym == 1 ? '(Anonim)' : $poster->name }}</b> dan menjadi tanggung jawab pengirim serta <b>tidak</b> mewakili {{ config('setting.site_name') }} dalam hal dan kepentingan apapun. Lihat <a target="_blank" href="{{ route('page','disclaimer') }}">Halaman Pernyataan</a></li>
                  <li>Pihak terlapor mungkin saja telah mengubah data untuk menghilangkan jejak</li>
                </ol>
              </p>
            </div>
          </div>
        <div class="comments-wrapper">
          <h2><span id="comment-count">{{ $comments->count() }}</span> Komentar</h2>
          <ul class="media-list">
            @foreach($comments->sortByDesc('id')->take(10)->reverse() as $comment)
            <li class="media comment-child" style="{{ $comment->deleted == 1 ? 'opacity:0.5' : '' }}" data-comment-id="{{ $comment->id }}">
              <div class="media-left"> <a href="#"> <img alt="image" src="http://www.viaviweb.in/envato/tf/viavi_directory_html/images/comment-thumb-1.jpg"> </a> </div>
              <div class="media-body">
                @if($comment->deleted != 1)
                  <p>
                    @if($comment->parent > 0)
                      @php
                        $parentComment = $comment->__parent;
                      @endphp
                      <span style="display:block;border:1px solid #ddd;padding: 7px 10px;margin-bottom: 10px;border-radius: 4px;">
                        <i>
                          <b>{{ $parentComment->__user->name }} : </b>
                          @if($parentComment->deleted != 1)
                            {!! substr($parentComment->content, 0, 100) !!}
                          @else
                            Komentar ini telah dihapus oleh {{ $parentComment->delete_by_user == 1 ? 'pengguna' : 'administrator' }}
                          @endif
                        </i></span>
                    @endif
                    {!! html_entity_decode($comment->content, ENT_QUOTES) !!}
                  </p>
                @else
                  <p><i>Komentar ini telah dihapus oleh {{ $comment->delete_by_user == 1 ? 'pengguna' : 'administrator' }}</i></p>
                @endif
                <div class="comment-meta clearfix"> <a href="#"><span class="author-name">{{ $comment->__user->name }}</span></a> <span class="comment-lt-time">{{ strftime('%d %b %Y %H:%M', strtotime($comment->created_at)) }}</span>
                  
                  @if($comment->deleted != 1)
                    @if($signed_in)
                      @if($signed_user->id == $comment->user_id)
                        <a class="delete-comment comment-reply-link pull-right cursor-pointer" data-comment-id="{{ $comment->id }}">Hapus</a>
                        <a class="update-comment comment-reply-link pull-right cursor-pointer" onclick="updateComment({{ $comment->id }});">Edit</a>
                      @else
                        <a class="report-comment comment-reply-link pull-right cursor-pointer" onclick="reportComment({{ $comment->id }});">Laporkan</a>
                        @if($post->__allowComment()))
                          <a class="reply-comment comment-reply-link pull-right cursor-pointer" onclick="replyComment({{ $comment->id }});">Balas</a>
                        @endif
                      @endif
                    @else
                      <a class="report-comment comment-reply-link pull-right cursor-pointer" onclick="reportComment({{ $comment->id }});">Laporkan</a>
                      @if($post->__allowComment()))
                        <a class="reply-comment comment-reply-link pull-right cursor-pointer" onclick="replyComment({{ $comment->id }});">Balas</a>
                      @endif
                    @endif
                  @endif

                </div>
              </div>
            </li>
            @endforeach
          </ul>
          <div class="comment-respond">
            <h2>Tulis Komentar</h2>
            @if($signed_in)
              @if($post->__allowComment())
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      Anda login sebagai : <b>{{ $signed_user->name }}</b>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea placeholder="" rows="3" class="form-control" id="comment-content"></textarea>
                </div>
                <button id="post-comment" class="btn pull-right" type="submit" onclick="postComment();">Kirim</button>
              @else
              <div class="dlt-com-lt-block">
                <div class="dlt-com-lt-text" style="padding-left:0px;text-align:center">
                  <p>Komentar dimatikan untuk postingan ini</p>
                </div>
              </div>
              @endif
            @else
            <div class="dlt-com-lt-block">
              <div class="dlt-com-lt-text" style="padding-left:0px;text-align:center">
                @php
                  $redirectURI = str_replace(url('/'), '', Request::fullUrl());
                @endphp
                <p>Anda harus <b><a href="{{ route('login') }}?redirect={{ base64_encode($redirectURI) }}">Masuk</a></b> atau <b><a target="_blank" href="{{ route('register') }}">Mendaftar</a></b> untuk mengirim komentar</p>
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 col-xs-12">
          <a id="url" class="btn green img-rounded align-center" style="width: 100%;border: solid 1px #ddd;color: #fff;background-color: #ffce10;text-align: center!important;border-radius: 6px!important;" href="#"><span style="font-size:22px">&nbsp; <span id="price">Rp {{ number_format($post->jumlah_kerugian, 0, '', '.') }}</span></span><br>Jumlah/Potensi Kerugian</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('/assets/js/colorbox/jquery.colorbox-min.js') }}"></script> 
<script src="{{ asset('/assets/js/bootstrap-select/bootstrap-select.min.js') }}"></script> 
<script src="{{ asset('/assets/js/owl.carousel/owl.carousel.js') }}"></script> 
<script src="{{ asset('/assets/js/superlist.js') }}"></script>

<script>

  function postComment() {
    var content = $("textarea[id=comment-content]").val();
    if( content != "" )
    {
      var oldButtonValue = $("button[id=post-comment]").html();
      $("button[id=post-comment]").attr('disabled', 'true').html('<i class="fa fa-spinner fa-md spinning"></i>');

      $.ajax({
        url: "{{ route('post.ajax.comments.new', ['id' => $post->id]) }}",
        type: "POST",
        dataType: "JSON",
        data: {
          content: content
        },
        success: function(s) {
          if( s.success === true )
          {
            var commentCount = parseInt($("#comment-count").text());
            $("#comment-count").html((commentCount+1));
            $("textarea[id=comment-content]").val("");

            var commentData = s.data.comment;
            var userData = s.data.user;
            var otherData = s.data.other;

            var commentHTML = '<li class="media comment-child" style="" data-comment-id="' + commentData.id + '">';
                commentHTML += '<div class="media-left"><a href="#"><img alt="image" src="http://www.viaviweb.in/envato/tf/viavi_directory_html/images/comment-thumb-1.jpg"></a></div>';
                commentHTML += '<div class="media-body">';
                commentHTML += '<p>' + commentData.content + '</p>';
                commentHTML += '<div class="comment-meta clearfix"> <a href="#"><span class="author-name">' + userData.name + '</span></a> <span class="comment-lt-time">' + otherData.human_readable_timestamp + '</span><a class="delete-comment comment-reply-link pull-right cursor-pointer" data-comment-id="' + commentData.id + '">Hapus</a><a class="update-comment comment-reply-link pull-right cursor-pointer" onclick="updateComment(' + commentData.id + ');">Edit</a></div>';
                commentHTML += '</div>';
                commentHTML += '</li>';


            $(".comments-wrapper").find('.media-list').append(commentHTML);

            $("button[id=post-comment]").removeAttr('disabled').html(oldButtonValue);
          }
          else
          {
            $("button[id=post-comment]").removeAttr('disabled').html(oldButtonValue);
            alert(s.error_message);
          }
        },
        error: function(e) {
          $("button[id=post-comment]").removeAttr('disabled').html(oldButtonValue);
          alert("error!");
        }
      });
    }
  }

  function updateComment(comment_id) {
    var comment_id = parseInt(comment_id);
    if( comment_id > 0 ) {

    }
  }

$('body').on("click", "a.delete-comment", function () {
    var comment_id = parseInt($(this).data("comment-id"));
        swal({
          title: "Anda yakin akan menghapus komentar ini",
          text: "Setelah dihapus komentar tidak dapat dikembalikan",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
           $.ajax({
              url: "{{ url('/post/'.$post->id.'/comments/delete') }}",
              type: "POST",
              dataType: "JSON",
              data: {
                comment_id: comment_id
              },
              success: function(s) {
                if( s.success === true ) {
                  $("li[data-comment-id=" + comment_id + "]").css({"opacity": "0.5"});
                  $("li[data-comment-id=" + comment_id + "]").find("a.delete-comment").remove();
                  $("li[data-comment-id=" + comment_id + "]").find("a.update-comment").remove();
                  $("li[data-comment-id=" + comment_id + "]").find(".media-body").find("p").html("<i>Komentar ini telah dihapus oleh pengguna</i>");

                  swal("Komentar berhasil dihapus!");
                }
                else {
                  swal("Gagal menghapus komentar!");
                }
              },
              error: function(e) {
                swal("Gagal menghapus komentar!");
              }
            }); 
          }
        });
    });

</script>

@endsection