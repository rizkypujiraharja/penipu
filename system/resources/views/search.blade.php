@extends('layouts.app')
@section('title', $keyword.' - Pencarian')
@section('content')
<div id="vfx-product-inner-item">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="news-search-lt">
          <input class="form-control" placeholder="Search" type="text">
          <span class="input-search"> <i class="fa fa-search"></i> </span> </div>
        <div class="left-slide-slt-block">
          <h3>Categories</h3>
        </div>
        <div class="list-group"> <a href="#" class="list-group-item active"><i class="fa fa-hand-o-right"></i> Business <span class="list-lt">15</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Health & Fitness <span class="list-lt">09</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Real Estate <span class="list-lt">18</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Entertainment <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Beauty & Spas <span class="list-lt">06</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Automotive <span class="list-lt">04</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Hotels & Travel <span class="list-lt">14</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Sports & Adventure <span class="list-lt">07</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Technology <span class="list-lt">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Arts & Entertainment <span class="list-lt">26</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Education & Learning <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Cloth Shop <span class="list-lt">16</span></a> </div>
        <div class="left-slide-slt-block">
          <h3>Popular Tags</h3>
        </div>
        <div class="archive-tag">
          <ul>
            <li><a href="#" class="active">Amazing</a></li>
            <li><a href="#">Envato</a></li>
            <li><a href="#">Themes</a></li>
            <li><a href="#">Clean</a></li>
            <li><a href="#">Responsivenes</a></li>
            <li><a href="#">SEO</a></li>
            <li><a href="#">Mobile</a></li>
            <li><a href="#">IOS</a></li>
            <li><a href="#">Flat</a></li>
            <li><a href="#">Design</a></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Location List</h3>
        </div>
        <div class="left-location-item">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Manchester</a><span class="list-lt">07</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Lankashire</a><span class="list-lt">04</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> New Mexico</a><span class="list-lt">03</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Nevada</a><span class="list-lt">06</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Kansas</a><span class="list-lt">08</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> West Virginina</a><span class="list-lt">05</span></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Archives</h3>
        </div>
        <div class="left-archive-categor">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> January 2016</a><span class="list-lt">09</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> February 2016</a><span class="list-lt">52</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> March 2016</a><span class="list-lt">36</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> April 2016</a><span class="list-lt">78</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> May 2016</a><span class="list-lt">66</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> June 2016</a><span class="list-lt">15</span></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
        <div class="col-md-12 col-sm-12 col-xs-12">

          <div class="sorts-by-results">
            <div class="col-md-6 col-sm-6 col-xs-6"> 
              <span class="result-item-view">
                Menampilkan 
                @if ($jumpage >= $page)
                  {{ $offset+1 .' - '.$jumpostinpage}}
                @else
                  0
                @endif
                dari 
                <span class="yellow">{{ $jumpos }}</span> Hasil
              </span> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="disp-f-right">
                <div class="disp-style"><a href="listing_grid.html"><i class="fa fa-th"></i></a></div>
                <div class="disp-style active"><a href="listing_list.html"><i class="fa fa-th-list"></i></a></div>
              </div>
            </div>
          </div>

          @if($jumpos > 0 && $jumpage >= $page)
            @foreach($posts as $post)
              @php
                $ss = json_decode($post->screenshot);
              @endphp
              <div class="recent-listing-box-container-item list-view-item">
                <div class="col-md-4 col-sm-12 nopadding feature-item-listing-item listing-item">
                  <div class="recent-listing-box-image">
                    <h1>{{ number_format_short($post->jumlah_kerugian) }}</h1>
                    <img src="{{ asset('/assets/images/post/'.$ss[0]) }}" alt="img1"> </div>
                  <div class="hover-overlay">
                    <div class="hover-overlay-inner">
                      <ul class="listing-links">
                        <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                        <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                        <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-sm-12 nopadding">
                  <div class="recent-listing-box-item">
                    <div class="listing-boxes-text"><a href="{{ route('post.view', ['id' => $post->id]) }}">
                      @php
                        $nama = json_decode($post->nama);
                      @endphp
                      <h3>{{ $nama[0] }}</h3>
                      <p>
                        @php
                          if( $by == 'nama' )
                          {
                            echo implode(", ", json_decode($post->nama, true));
                            echo "<br/>";
                          }
                          elseif($by == 'rekening')
                          {
                            $reks = json_decode($post->rekening_bank);
                            foreach($reks as $rek)
                            {
                                echo $rek->nama_bank." ".$rek->nomor_rekening." A/N ".$rek->nama_pemilik." | ";
                            }
                            echo "<br/>";
                          }
                          elseif( $by == 'telepon' )
                          {
                            echo implode(", ", json_decode($post->nomor_telepon, true));
                            echo "<br/>";
                          }
                          elseif( $by == 'email' )
                          {
                            echo implode(", ", json_decode($post->email, true));
                            echo "<br/>";
                          }
                        @endphp
                        {!! substr(strip_tags(html_entity_decode($post->kronologi, ENT_QUOTES)), 0, 200) !!}
                      </p></a>
                    </div>
                    <div class="recent-feature-item-rating">
                      <h2><i class="fa fa-calendar"></i> {{ strftime("%d %b %Y %H:%M", strtotime($post->created_at)) }}</h2>
                      <span>  </span> 
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          @else
            Not found
          @endif
        </div>
         @if($jumpos > 0)
          <div class="vfx-person-block">
            
          <center>
            Page {{ $page }}
          </center>
            <ul class="vfx-pagination">
              <li><a href="{{$request->fullUrlWithQuery(['page' => 1])}}"><i class="fa fa-angle-double-left"></i></a></li>
              @php
                if ($page <= 3 || $jumpage < $page) {
                  $start = 1;
                  if($jumpage < 5){
                    $end = $jumpage;
                  }else{
                    $end = 5;
                  }
                } else if ($jumpage - 2 < $page) {
                  $start = $jumpage - 4;
                  $end = $jumpage;
                }else{
                  $start = $page - 2;
                  $end = $start + 4;
                }
              @endphp
            @for ($i = $start; $i <= $end; $i++)
              <li class="{{$i == $page ? 'active' : ''}}"><a href="{{$request->fullUrlWithQuery(['page'=>$i])}}">{{$i}}</a></li>
            @endfor
              <li><a href="{{$request->fullUrlWithQuery(['page' => $jumpage])}}"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
          </div>
         @endif
      </div>
    </div>
  </div>
</div>
@endsection