<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

Route::group(['middleware' => ['web']], function() {
	
	Route::get('/', 'HomeController@index')->name('home');
	Route::post('/ajax-search', 'HomeController@ajaxSearch')->name('home.ajax-search')->middleware(['xmlhttprequest']);
	Route::get('/search', 'HomeController@search')->name('search');
	Route::get('/post/view/{id}', 'PostController@view')->name('post.view');

	Route::group(['prefix' => 'post/{id}/comments'], function() {
		Route::post('/load', 'PostController@ajaxLoadComment')->name('post.ajax.comments.load')->middleware(['xmlhttprequest']);
		Route::post('/new', 'PostController@ajaxNewComment')->name('post.ajax.comments.new')->middleware(['auth', 'xmlhttprequest']);
		Route::post('/update', 'PostController@ajaxUpdateComment')->name('post.ajax.comments.update')->middleware(['auth', 'xmlhttprequest']);
		Route::post('/delete', 'PostController@ajaxDeleteComment')->name('post.ajax.comments.delete')->middleware(['auth', 'xmlhttprequest']);
	});
	

	Route::get('/page/{slug}', 'PageController@show')->name('page');

	## Auth
	Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
		Route::get('/login', 'LoginController@showLoginForm')->name('login');
		Route::post('/login', 'LoginController@login')->name('login.proccess');
		Route::get('/logout', 'LoginController@logout')->name('logout');
		Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
		Route::post('/register', 'RegisterController@register')->name('register.proccess');
		Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
		Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
		Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm');
		Route::post('/password/reset', 'ResetPasswordController@reset');
	});


	## Account ##
	Route::group(['middleware' => 'auth', 'namespace' => 'Account', 'prefix' => 'account'], function() {
		Route::get('/activation', 'ActivationController@index')->name('account.activation');
		Route::get('/home', 'HomeController@index')->name('account.home');
		Route::get('/report/new', 'PostController@new')->name('account.newreport');
		Route::post('/report/new', 'PostController@store')->name('account.newreport.store');
		Route::get('/report/edit/{id}', 'PostController@edit')->name('account.report.edit');
		Route::post('/report/edit/{id}', 'PostController@update')->name('account.report.update');
		Route::get('/report/delete/{id}', 'PostController@delete')->name('account.report.delete');
		Route::get('/password', 'ProfileController@changePasswordForm')->name('account.changepassword');
		Route::post('/password', 'ProfileController@changePasswordStore')->name('account.changepassword.store');
		Route::get('/verification', 'VerificationController@form')->name('account.verification');
		Route::post('/verification', 'VerificationController@store')->name('account.verification.store');
		Route::get('/profile', 'ProfileController@index')->name('account.profile');
		Route::post('/profile', 'ProfileController@store')->name('account.profile.store');
	});


	# Admin
	Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:admin']], function() {
		Route::get('/', 'HomeController@index')->name('admin.home');
		Route::group(['prefix' => 'posts'], function() {
			Route::get('/', 'PostController@index')->name('admin.posts.index');
			Route::get('/edit/{id}', 'PostController@edit')->name('admin.posts.edit');
			Route::post('/edit/{id}', 'PostController@update')->name('admin.posts.update');
			Route::get('/preview/{id}', 'PostController@preview')->name('admin.posts.preview');
			Route::get('/delete/{id}', 'PostController@delete')->name('admin.posts.delete');
			Route::post('/ajax/search', 'PostController@ajaxSearch')->name('admin.posts.ajax.search');
		});
		Route::group(['prefix' => 'verification'], function() {
			Route::get('/', 'VerificationController@index')->name('admin.verification');
			Route::get('view/{id}', 'VerificationController@show')->name('admin.verification.show');
			Route::post('action/{id}', 'VerificationController@action')->name('admin.verification.action');
		});

		Route::group(['prefix' => 'comment'], function(){
			Route::get('/', 'CommentController@index')->name('comment.index');
			Route::delete('/delete/{comment}', 'CommentController@delete')->name('comment.delete');
		});

		Route::resource('page', 'PageController');

		Route::get('/setting', 'SettingController@setting')->name('setting');
		Route::post('/setting/update', 'SettingController@update')->name('setting.update');
	});


	## Misc
	Route::get('/robots.txt', 'MiscController@robots')->name('robots.txt');
	Route::get('/sitemap.xml', 'MiscController@sitemapIndex')->name('sitemap.xml');

});