<?php

function number_format_short( $n, $precision = 1 ) {
	if ($n < 900) {
		// 0 - 900
		$n_format = number_format($n, $precision, ',', '');
		$suffix = '';
	} else if ($n < 900000) {
		// 0.9k-850k
		$n_format = number_format($n / 1000, $precision, ',', '');
		$suffix = 'Rb';
	} else if ($n < 900000000) {
		// 0.9m-850m
		$n_format = number_format($n / 1000000, $precision, ',', '');
		$suffix = 'Jt';
	} else if ($n < 900000000000) {
		// 0.9b-850b
		$n_format = number_format($n / 1000000000, $precision, ',', '');
		$suffix = 'M';
	} else {
		// 0.9t+
		$n_format = number_format($n / 1000000000000, $precision, ',', '');
		$suffix = 'T';
	}
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
	if ( $precision > 0 ) {
		$dotzero = ',' . str_repeat( '0', $precision );
		$n_format = str_replace( $dotzero, '', $n_format );
	}
	return $n_format . $suffix;
}

function front_path($dir="")
{
	$d = dirname(__DIR__, 3)."/public_html";
	return rtrim($d."/".ltrim($dir, "/"), "/");
}

function round_up($number, $precision = 2)
{
    $fig = (int) str_pad('1', $precision, '0');
    return (ceil($number * $fig) / $fig);
}

function round_down($number, $precision = 2)
{
    $fig = (int) str_pad('1', $precision, '0');
    return (floor($number * $fig) / $fig);
}

function userDate($time, $format, $timezone = 'Asia/Jakarta')
{
	$timestamp = is_numeric($time) ? $time : strtotime($time);
	$datetime = new \DateTime("@$timestamp");
	$datetimezone = new \DateTimeZone($timezone);
	$datetime->setTimezone($datetimezone);
	return $datetime->format($format);
}

?>