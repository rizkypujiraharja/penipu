<?php

namespace App\Providers;

use Config, View, Schema, Validator, Hash;
use App\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        if (Schema::hasTable('settings')) {
            foreach (Setting::all() as $setting) {
                $name = $setting->name;
                $val = $setting->is_json == 1 ? json_decode($setting->value, true) : $setting->value;
                Config::set('setting.'.$name, $val);
            }
        }
        
        View::share('config', Config::all());

        Validator::extend('passcheck', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}