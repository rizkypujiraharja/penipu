<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	const STATUS_DRAFT = 0;
	const STATUS_REVIEW = 1;
	const STATUS_PUBLIC = 2;
	const STATUS_HOLD = 3;
	const STATUS_REJECTED = 4;

	const DISALLOW_COMMENT = 0;
	const ALLOW_COMMENT = 1;

	const UNANONYM = 0;
	const ANONYM = 1;

	protected $table = 'posts';

	public function __user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function __comments()
	{
		return $this->hasMany('App\Models\Comment', 'post_id');
	}

	public function __allowComment($id = 0)
	{
		if( $id > 0 )
		{
			$gp = DB::table($this->table)
						->where('id', $id)
						->where('izinkan_komentar', self::ALLOW_COMMENT)
						->count();

			return ($gp > 0 ? true : false);
		}
		elseif( !empty($this->id) )
		{
			return ($this->izinkan_komentar == self::ALLOW_COMMENT ? true : false);
		}

		return false;
	}

	public function __status($id = 0)
	{
		if( $id > 0 )
		{
			$sql = DB::table($this->table)
						->select('status')
						->where('id', $id)
						->first();

			if( !$sql )
				return null;

			return $sql->status;
		}
		elseif( !empty($this->id) )
		{
			$status = $this->status;
		}

		return $status;
	}

	public function getConstant($name)
	{
		switch($name)
		{
			case 'STATUS_DRAFT':
				return self::STATUS_DRAFT;
				break;

			case 'STATUS_REVIEW':
				return self::STATUS_REVIEW;
				break;

			case 'STATUS_PUBLIC':
				return self::STATUS_PUBLIC;
				break;

			case 'STATUS_HOLD':
				return self::STATUS_HOLD;
				break;

			case 'STATUS_REJECTED':
				return self::STATUS_REJECTED;
				break;

			case 'DISALLOW_COMMENT':
				return self::DISALLOW_COMMENT;
				break;

			case 'ALLOW_COMMENT':
				return self::ALLOW_COMMENT;
				break;

			case 'UNANONYM':
				return self::UNANONYM;
				break;

			case 'ANONYM':
				return self::ANONYM;
				break;

			default:
				return null;
				break;
		}
	}
}