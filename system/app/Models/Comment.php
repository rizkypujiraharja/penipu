<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = ['deleted', 'delete_by_admin'];
	
	public function __user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function __post()
	{
		return $this->belongsTo('App\Models\Post', 'post_id');
	}

	public function __parent()
	{
		return $this->belongsTo('App\Models\Comment', 'parent');
	}
}