<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
	const STATUS_REVIEW = 0;
	const STATUS_APPROVED = 1;
	const STATUS_REJECTED = 2;
	
	protected $fillable =['additional_info', 'status'];

	protected $table = 'user_verifications';

	protected $primaryKey = 'user_id';

	public function __user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function getConstant($name)
	{
		switch($name)
		{
			case 'STATUS_REVIEW':
				return self::STATUS_REVIEW;
				break;

			case 'STATUS_APPROVED':
				return self::STATUS_APPROVED;
				break;

			default:
				return null;
				break;
		}
	}
}