<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Page;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::latest()->get();
        
        return view('admin.pages.index', compact('pages'));
    }

    public function show(Page $page)
    {
        return view('admin.pages.show', compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|unique:pages,title|max:170',
            'content' => 'required|string',
            'status'  => 'required|integer|max:1'
        ],[
            'title.required' => 'Judul tidak boleh kosong',
            'title.string'  => 'Judul tidak valid',
            'title.unique'  => 'Judul sudah pernah dibuat',
            'title.max'     => 'Judul tidak boleh lebih dari 100 karakter',
            'content.required'  => 'Content tidak boleh kosong',
            'content.string'    => 'Content tidak valid',
            'status'   => 'Status tidak valid'
        ]);

        $content = str_replace(["<script", "</script>", "<style", "</style>"], '', $request->content);
        $content = htmlentities($content, ENT_QUOTES);

        $page = Page::create([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'content' => $content,
            'status' => $request->status,
        ]);

        return redirect()->route('page.index')->with('alert-success', 'Halaman berhasil dibuat');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Page $page)
    {
        $this->validate($request, [
            'title' => 'required|string|max:100|unique:pages,title,'.$page->id,
            'content' => 'required|string',
            'status'  => 'required|integer|max:1'
        ],[
            'title.required' => 'Judul tidak boleh kosong',
            'title.string'  => 'Judul tidak valid',
            'title.unique'  => 'Judul sudah pernah dibuat',
            'title.max'     => 'Judul tidak boleh lebih dari 100 karakter',
            'content.required'  => 'Content tidak boleh kosong',
            'content.string'    => 'Content tidak valid',
            'status'   => 'Status tidak valid'
        ]);

        $content = str_replace(["<script", "</script>", "<style", "</style>"], '', $request->content);
        $content = htmlentities($content, ENT_QUOTES);

        $page->update([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'content' => $content,
            'status' => $request->status,
        ]);

        return redirect()->route('page.index')->with('alert-success', 'Halaman berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()->back()->with('alert-success', 'Halaman berhasil dihapus');
    }
}
