<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Auth, Validator, DB;

class PostController extends Controller
{
	

	public function delete(Request $request, $id)
    {
        $id = intval($id);

        if($id > 0)
        {
            $del = Post::where('user_id', Auth::user()->id)
                        ->findOrFail($id);

            $imageLoc = front_path('/assets/images/post');
            	$sss = json_decode($del->screenshot, true);
	            foreach ($sss as $ss) {
	            	!file_exists($imageLoc."/".$ss) or unlink($imageLoc."/".$ss);
	            }
	        $del->delete();
            if( $del )
            {
                return redirect()->back()->with('flash-success', 'Berhasil menghapus laporan #'.$id);
            }
        }

        return redirect()->back()->with('flash-error', 'Gagal menghapus laporan #'.$id);
    }

	public function index(Request $request)
	{
		
		$sql = Post::where('id', '>', 0);

		if( isset($request->search) )
		{
			$id = $request->id;
			$preview = $request->preview;
			$date = $request->date;
			$status = $request->status != 'all' ? intval($request->status) : null;

			if( !is_null($id) )
			{
				$sql = $sql->where('id', $id);
			}

			if( !empty($preview) )
			{

			}

			if( !empty($date) )
			{
				$d = explode("-", $date);
				$day = $d[0];
				$month = $d[1];
				$year = $d[2];

				$_Start = strtotime("$year-$month-$day 00:00:00") - (7*60*60);
				$_End = $_Start + (24*60*60);

				$dateStart = date('Y-m-d H:i:s', $_Start);
				$dateEnd = date('Y-m-d H:i:s', $_End);

				$sql = $sql->where('created_at', '>=', $dateStart)
							->where('created_at', '<=', $dateEnd);
			}

			if( !is_null($status) )
			{
				$sql = $sql->where('status', $status);
			}
		}


		$limit = 10;
        $page = $request->page ?? 1 ;
        $offset = ($page * $limit) - ($limit) ;
        $jumpos = $sql->count();
        $jumpage = intval(ceil($jumpos / $limit));
		$posts = $sql->offset($offset)->limit($limit)->get();
		
		return view('admin.posts.index', compact('posts','request', 'jumpage', 'page'));
	}

	public function edit($post)
    {
    	$post = Post::where('user_id', Auth::user()->id)->findOrFail($post);
    	return view('admin.posts.edit-report', compact('post'));
    }

	public function update(Request $request, $id)
    {

    	$nama = $request->input('nama');
		$rekening_bank = $request->input('rekening_bank');
		$alamat = $request->input('alamat');
		$nomor_telepon = $request->input('nomor_telepon');
		$email = $request->input('email');
		$media_sosial = $request->input('media_sosial');
		$jumlah_kerugian = @intval($request->input('jumlah_kerugian'));
		$kronologi = $request->input('kronologi');

		$error = [];

		$i = 0;
		foreach($nama as $_nama)
		{
			if( empty($_nama) )
			{
				$error['nama'][$i] = 'Kolom tidak boleh kosong atau hapus untuk menghilangkan';
			}

			$i++;
		}
		if( !is_array($nama) ) $nama = [];

		for($i=0; $i<count($rekening_bank['nama_bank']); $i++)
		{
			$banks[] = [
				'nama_bank'			=> $rekening_bank['nama_bank'][$i],
				'nomor_rekening'	=> $rekening_bank['nomor_rekening'][$i],
				'nama_pemilik'		=> $rekening_bank['nama_pemilik'][$i]
			];
		}

		$rekening_bank = $banks;

		$i = 0;
		foreach($rekening_bank as $_rekening_bank)
		{
			if( empty($_rekening_bank['nama_bank']) || empty($_rekening_bank['nomor_rekening']) || empty($_rekening_bank['nama_pemilik']) )
			{
				$error['rekening_bank'][$i] = 'Sebagian data masih ada yang kosong';
			}
			else
			{
				$rekening_bank[$i] = [
					'nama_bank'			=> $_rekening_bank['nama_bank'],
					'nomor_rekening'	=> preg_replace('/[^0-9]/', '', $_rekening_bank['nomor_rekening']),
					'nama_pemilik'		=> $_rekening_bank['nama_pemilik']
					];
			}

			$i++;
		}
		if( !is_array($rekening_bank) ) $rekening_bank = [];

		$alamat = empty($alamat) ? '' : $alamat;

		$i = 0;
		foreach($nomor_telepon as $_nomor_telepon)
		{
			if( empty($_nomor_telepon) )
			{
				unset($nomor_telepon[$i]);
			}
			else
			{
				$nomor_telepon[$i] = str_replace([' ', '-'], '', $nomor_telepon[$i]);
			}

			$i++;
		}
		if( !is_array($nomor_telepon) ) $nomor_telepon = [];

		$i = 0;
		foreach($email as $_email)
		{
			if( empty($_email) )
			{
				unset($email[$i]);
			}
			else 
			{
				$email[$i] = str_replace(' ', '', $email[$i]);
				if( !filter_var($email[$i], FILTER_VALIDATE_EMAIL) )
				{
					$error['email'][$i] = 'Format email tidak valid';
				}
			}

			$i++;
		}
		if( !is_array($email) ) $email = [];

		$i = 0;
		foreach($media_sosial as $_media_sosial)
		{
			if( empty($_media_sosial) )
			{
				unset($media_sosial[$i]);
			}
			else
			{
				if( !filter_var($media_sosial[$i], FILTER_VALIDATE_URL) )
				{
					$error['media_sosial'][$i] = 'Format URL tidak valid';
				}
			}

			$i++;
		}
		if( !is_array($media_sosial) ) $media_sosial = [];

		$kronologi = str_replace(["<script", "</script>", "<style", "</style>"], '', $kronologi);
		$kronologi = htmlentities($kronologi, ENT_QUOTES);
		if( strlen($kronologi) <= 300 ) $error['kronologi'] = 'Minimal 300 karakter';
		
		$jmlss = $request->oldss ;
		$ss = $request->file('screenshot');
		$imageLoc = front_path('/assets/images/post');
		$screenshot = [];
		$i = 0;
		try
		{
			if( $ss == null && $jmlss == 0){
				throw new \Exception("Silahkan upload minimal 1 gambar", $i);
			}else if($ss != null){
				foreach($ss as $scr)
				{
					if( !$scr->isValid() )
						throw new \Exception("Gambar tidak valid", $i);
					
					$ext = str_replace('image/', '', $scr->getClientOriginalExtension());
					if( !in_array($ext, ['png', 'jpg', 'jpeg']) )
						throw new \Exception("Format gambar tidak valid, format: png, jpg, jpeg", $i);

					if( $scr->getClientSize() > 1030000 )
						throw new \Exception("Maksimal ukuran file yang diperbolehkan adalah 1 Mb", $i);
					
					$newname = time().uniqid().md5(time()).".".$ext;
					if( file_exists($imageLoc."/".$newname) )
						throw new \Exception("Kesalahan saat mengunggah gambar", $i);
								
					$scr->move($imageLoc, $newname);
					if( !file_exists($imageLoc."/".$newname) || !is_file($imageLoc."/".$newname) )
						throw new \Exception("Failed to upload file", $i);

					$screenshot[] = $newname;

					$i++;
				}
			}

			if($jmlss != NULL){
				foreach ($request->oldss as $oldss) {
					$screenshot[] = $oldss;
				}
			}
		}
		catch(\Exception $e)
		{
			$error['screenshot'] = "Gambar ".$e->getCode()." error : ".$e->getMessage();
			foreach($screenshot as $u)
			{
				!file_exists($imageLoc."/".$u) or unlink($imageLoc."/".$u);
			}
		}

 		// Delete Image
		if($request->delete_img != NULL){
	 		$post=Post::where('user_id', Auth::user()->id)->findOrFail($id);
	    	$oldss = json_decode($post->screenshot);
			$imageLoc = front_path('/assets/images/post');
			foreach ($request->delete_img as $delete_img) {
				if (in_array($delete_img, $oldss)){
		    		!file_exists($imageLoc."/".$delete_img) or unlink($imageLoc."/".$delete_img);
		    	}
			}
		}

		if( count($error) == 0 )
		{
			DB::beginTransaction();

			try
			{
				$post = Post::findOrFail($id);
				$post->anonym = Post::UNANONYM;
				$post->nama = json_encode($nama);
				$post->rekening_bank = json_encode($rekening_bank);
				$post->alamat = $alamat;
				$post->nomor_telepon = json_encode($nomor_telepon);
				$post->email = json_encode($email);
				$post->media_sosial = json_encode($media_sosial);
				$post->jumlah_kerugian = $jumlah_kerugian;
				$post->screenshot = json_encode($screenshot);
				$post->kronologi = $kronologi;
				$post->izinkan_komentar = Post::ALLOW_COMMENT;
				$post->status = $request->status;
				$post->save();

				DB::commit();

				return redirect()->back()->with('alert-success', 'Laporan telah berhasil diupdate');
			}
			catch(\PDOException $pdo)
			{
				DB::rollBack();
				foreach($screenshot as $u)
				{
					!file_exists($imageLoc."/".$u) or unlink($imageLoc."/".$u);
				}

				return redirect()->back()->with('alert-error', 'Internal service error')->withInput();
			}
			catch(\Exception $e)
			{
				DB::rollBack();
				foreach($screenshot as $u)
				{
					!file_exists($imageLoc."/".$u) or unlink($imageLoc."/".$u);
				}

				return redirect()->back()->with('flash-error', $e->getMessage())->withInput();
			}
		}
		else
		{
			foreach($screenshot as $u)
			{
				!file_exists($imageLoc."/".$u) or unlink($imageLoc."/".$u);
			}

			return redirect()->back()->with(['alert-error' => 'Periksa ulang data yang Anda masukkan', 'form_validation_error' => $error])->withInput();
		}
	}

	

	public function preview(Request $request, $id)
	{
		$id = intval($id);
		$post = Post::findOrFail($id);
		$poster = $post->__user;
		$comments = $post->__comments;
		$nama = json_decode($post->nama);
		$rekening_bank = json_decode($post->rekening_bank);
		$nomor_telepon = json_decode($post->nomor_telepon);
		$email = json_decode($post->email);
		$media_sosial = json_decode($post->media_sosial);
		$screenshot = json_decode($post->screenshot);

		return view('admin.posts.view', compact('post', 'poster', 'nama', 'rekening_bank', 'nomor_telepon', 'email', 'media_sosial', 'screenshot', 'comments'));
	}
}