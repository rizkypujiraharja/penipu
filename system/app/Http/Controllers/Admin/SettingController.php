<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
	public function setting()
	{
		return view('admin.setting', compact('setting'));
	}
}