<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CommentController extends Controller
{
    public function index(Request $request)
    {
    	$comments = Comment::select('users.name', 'comments.id', 'comments.post_id', 'comments.content', 'comments.deleted')
						->leftJoin('users', 'users.id', '=', 'comments.user_id')
						->orderBy('comments.id', 'DESC');

		if( isset($request->post_id) ){
			$post_id = $request->post_id;
			$comments = $comments->where('comments.id', $post_id);
		}

		if( isset($request->nama) ){
			$nama = '%'.$request->nama.'%';
			$comments = $comments->where('users.name', 'like', $nama);
		}

		if( isset($request->deleted) && $request->deleted !='all'){
			$comments = $comments->where('comments.deleted', $request->deleted);
		}

		$limit = 10;
        $page = $request->page ?? 1 ;
        $offset = ($page * $limit) - ($limit) ;
        $jumpos = $comments->count();
        $jumpage = intval(ceil($jumpos / $limit));
		$comments = $comments->offset($offset)->limit($limit)->get();

		return view('admin.comment.index', compact('comments', 'request', 'jumpos', 'jumpage', 'page'));
    }

    public function delete(Comment $comment)
    {
    	$comment->update([
    		'deleted' => 1,
    		'delete_by_admin' => 1,
    	]);

    	return redirect()->back()->with('alert-success', 'Komentar berhasil dihapus');
    }
}
