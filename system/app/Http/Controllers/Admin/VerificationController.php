<?php

namespace App\Http\Controllers\Admin;

use Auth, DB, Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserVerification;

class VerificationController extends Controller
{
	
	public function index(Request $request)
	{
		$verifications = UserVerification::select('users.id', 'users.name', 'user_verifications.status', 'user_verifications.created_at')
						->leftJoin('users', 'users.id', '=', 'user_verifications.user_id')->orderBy('user_verifications.status', 'ASC')->orderBy('user_verifications.created_at', 'DESC');

		if( isset($request->nama) ){
			$nama = '%'.$request->nama.'%';
			$verifications = $verifications
				->where('users.name', 'like', $nama);
		}

		if( isset($request->status) && $request->status !='all'){
			$verifications = $verifications
				->where('user_verifications.status', $request->status);
		}
		if( isset($request->id) && $request->id != Null ){
			$verifications = $verifications
				->where('users.id', $request->id);
		}


		$limit = 10;
        $page = $request->page ?? 1 ;
        $offset = ($page * $limit) - ($limit) ;
        $jumpos = $verifications->count();
        $jumpage = intval(ceil($jumpos / $limit));
		$verifications = $verifications->offset($offset)->limit($limit)->get();

		return view('admin.verification.index', compact('verifications', 'request', 'jumpos', 'jumpage', 'page'));
	}

	public function show($id)
	{
		$userverif = UserVerification::findOrFail($id);
		
		return view('admin.verification.show', compact('userverif'));
	}

	public function action(Request $request, $id)
	{
		$this->validate($request, [
			'reason' => 'required|string|min:10|max:255'
		],[
			'reason.required'	=> 'Alasan tidak boleh kosong',
			'reason.string' 	=> 'Alasan tidak valid',
			'reason.min'		=> 'Masukkan alasan minimal 10 karakter',
			'reason.max'		=> 'Masukkan alasan maxksmal 255 karakter'
		]);
		$userverif = UserVerification::findOrFail($id);
		
		if($request->action == 'Approve'){
			$user=User::findOrFail($id)->update([
				'verified_member' => 1
			]);

			$userverif->update([
				'additional_info' => $request->reason,
				'status' => 1
			]);

			$message = 'Verifikasi berhasil disetujui';

		}else if($request->action == 'Non Approve'){
			$photo = $userverif->photo;
			$document = $userverif->id_document;

			@unlink(public_path('/assets/images/user-verification').'/'.$photo);
			@unlink(public_path('/assets/images/user-verification').'/'.$document);

			$userverif->delete();

			$user=User::findOrFail($id)->update([
				'verified_member' => 0
			]);

			$message = 'Verifikasi berhasil ditolak';
		}

		return redirect()->route('admin.verification')->with('alert-success', $message);
	}
	
}