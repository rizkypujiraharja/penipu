<?php

namespace App\Http\Controllers\Account;

use DB, Auth, Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserVerification;

class VerificationController extends Controller
{
	public function form()
	{
		$user = Auth::user();
		$verification = $user->__verificationData;
		return view('account.verification', compact('user', 'verification'));
	}

	public function store(Request $request)
	{
		$validation = Validator::make($request->all(), [
			'nama'					=> 'required|string',
			'alamat'				=> 'required|string',
			'kota'					=> 'required|string',
			'provinsi'				=> 'required|string',
			'kode_pos'				=> 'required',
			'photo'					=> 'required|image|mimes:jpeg,jpg,png|max:1024',
			'id_document'			=> 'required|image|mimes:jpeg,jpg,png|max:1024',
			], [
			'nama.required'			=> 'Nama tidak boleh kosong',
			'nama.string'			=> 'Nama tidak valid',
			'alamat.required'		=> 'Alamat tidak boleh kosong',
			'alamat.string'			=> 'Alamat tidak valid',
			'kota.required'			=> 'Kota tidak boleh kosong',
			'kota.string'			=> 'Kota tidak valid',
			'provinsi.required'		=> 'Provinsi tidak boleh kosong',
			'provinsi.string'		=> 'Provinsi tidak valid',
			'kode_pos.required'		=> 'Kode Pos tidak boleh kosong',
			'kode_pos.string'		=> 'Kode pos tidak valid',
			'photo.required'		=> 'Foto setengah badan tidak boleh kosong',
			'photo.image'			=> 'Foto setengah badan harus berupa gambar',
			'photo.mimes'			=> 'Format foto setengah badan yang diperbolehkan adalah jpeg, jpg, dan png',
			'photo.max'				=> 'Maksimal ukuran foto setengah badan adalah 1.024 Kb (1Mb)',
			'id_document.required'	=> 'KTP tidak boleh kosong',
			'id_document.image'		=> 'KTP harus berupa gambar',
			'id_document.mimes'		=> 'Format KTP yang diperbolehkan adalah jpeg, jpg, dan png',
			'id_document.max'		=> 'Maksimal ukuran KTP adalah 1.024 Kb (1Mb)',
			]);

		if( $validation->fails() )
		{
			return redirect()->back()->with('alert-error', 'Periksa data yang Anda masukkan')->withErrors($validation)->withInput();
		}

		$user = Auth::user();
		$fileLocation = public_path('/assets/images/user-verification');

		if( $request->hasFile('photo') )
		{
			$filePhoto = $request->file('photo');
			$photoName = $user->id.uniqid().md5(time()).mt_rand(100, 1000).'.'.strtolower($filePhoto->getClientOriginalExtension());
			$newPhoto = $fileLocation.'/'.$photoName;

			if( file_exists($newPhoto) && is_file($newPhoto) )
				return redirect()->back()->with('flash-error', 'Gagal mengunggah foto setengah badan. Terjadi kesalahan sistem silahkan diulangi')->withInput();

			if( !$filePhoto->move($fileLocation, $photoName) )
				return redirect()->back()->with('flash-error', 'Gagal mengunggah foto setengah badan')->withInput();
		}

		if( $request->hasFile('id_document') )
		{
			$fileIDDocument = $request->file('id_document');
			$idDocumentName = $user->id.uniqid().md5(time()).mt_rand(100, 1000).'.'.strtolower($fileIDDocument->getClientOriginalExtension());
			$newIDDocument = $fileLocation.'/'.$idDocumentName;

			if( file_exists($newIDDocument) && is_file($newIDDocument) ) {
				@unlink($fileLocation.'/'.$photoName);
				return redirect()->back()->with('flash-error', 'Gagal mengunggah KTP. Terjadi kesalahan sistem silahkan diulangi')->withInput();
			}

			if( !$fileIDDocument->move($fileLocation, $idDocumentName) ) {
				@unlink($fileLocation.'/'.$photoName);
				return redirect()->back()->with('flash-error', 'Gagal mengunggah KTP')->withInput();
			}
		}

		DB::beginTransaction();

		try
		{
			if( !is_dir($fileLocation) || !file_exists($fileLocation) )
				throw new \Exception("Terjadi kesalahan sistem. Silahkan hubungi Administrator");

			$verification = UserVerification::where('user_id', $user->id)->first();
			if( $verification )
				throw new \Exception("Gagal mengirim data. Anda sudah pernah mengirimkan permintaan verifikasi");

			$verification = new UserVerification;
			$verification->user_id = $user->id;
			$verification->photo = $photoName;
			$verification->id_document = $idDocumentName;
			$verification->status = UserVerification::STATUS_REVIEW;
			$verification->additional_info = 'Verifikasi akun sedang dalam proses review';
			if( !$verification->save() )
				throw new \Exception("Gagal mengirim data. Terjadi kesalahan sistem silahkan diulangi");

			$user = User::find($user->id);
			$user->name = $request->nama;
			$user->alamat = $request->alamat;
			$user->kota = $request->kota;
			$user->provinsi = $request->provinsi;
			$user->kode_pos = $request->kode_pos;
			if( !$user->save() )
				throw new \Exception("Gagal mengirim data. Terjadi kesalahan sistem silahkan diulangi");

			DB::commit();

			return redirect()->back()->with('alert-success', 'Permintaan verifikasi telah diterima. Tim kami akan memeriksa dan melakukan validasi data yang Anda kirim maksimal 3x24 jam kedepan');
		}
		catch(\PDOException $pdo)
		{
			DB::rollBack();

			@unlink($fileLocation.'/'.$photoName);
			@unlink($fileLocation.'/'.$idDocumentName);

			return redirect()->back()->with('flash-error', 'Terjadi kesalahan sistem. Silahkan hubungi Administrator')->withInput();
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			@unlink($fileLocation.'/'.$photoName);
			@unlink($fileLocation.'/'.$idDocumentName);

			return redirect()->back()->with('flash-error', $e->getMessage())->withInput();
		}
	}


}