<?php

namespace App\Http\Controllers\Account;

use Auth, Validator, Hash;
use App\Http\Controllers\Controller;
use App\Setting;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function index()
	{
		$id = Auth::user()->id;
		$user = User::find($id);
		return view('account.profile.edit-profile', compact('user'));
	}

	public function store(Request $request)
	{
		
		$this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|email|unique:users,email|max:100',
            'alamat' => 'required|string',
            'kota' => 'required|string|max:50',
            'provinsi' => 'required|string|max:50',
            'kode_pos' => 'required|numeric',
        ],[
        	'name.required' => 'Nama tidak boleh kosong',
        	'name.string' => 'Nama tidak valid',
        	'name.max' => 'Nama tidak boleh lebih dari 100 karakter',
        	'email.required' => 'Email tidak boleh kosong',
        	'email.email' => 'Email tidak valid',
        	'email.unique' => 'Email sudah digunakan',
        	'email.max' => 'Emaiil tidak boleh lebih dari 100 karakter',
        	'alamat.required' => 'Alamat tidak boleh kosong',
        	'alamat.string' => 'Alamat tidak valid',
        	'kota.required' => 'Kota tidak boleh kosong',
        	'kota.string' => 'Kota tidak valid',
        	'kota.max' => 'Kota tidak boleh lebih dari 50 karakter',
        	'provinsi.required' => 'Provinsi tidak boleh kosong',
        	'provinsi.string' => 'Provinsi tidak valid',
        	'provinsi.max' => 'Provinsi tidak boleh lebih dari 50 karakter',
        	'kode_pos.required' => 'Kode Pos tidak boleh kosong',
        	'kode_pos.numeric' => 'Kode Pos tidak valid'
        ]);

		$id = Auth::user()->id;
		$user = User::find($id);
		if($user->verfied_member != 1){
			$new = ['email' => $request->email];
		}else{
			$new = [
				'name' => $request->name,
				'email' => $request->email,
				'alamat' => $request->alamat,
				'kota' => $request->kota,
				'provinsi' => $request->provinsi,
				'kode_pos' => $request->kode_pos
			];
		}

		if($user->email != $request->email){
			$new['verified_email'] = 0;
		}

		$user->update($new);

		if($new['verified_email']==0){
			Auth::logout();
			return redirect()->route('login')->with('alert-success', 'Email berhasil diubah silahkan cek email untuk melakukan verifikasi ulang email Anda.');
		}

		return redirect()->back()->with('alert-success', 'Prfile berhasil diperbarui');

	}

	public function changePasswordForm()
	{
		return view('account.profile.change-password');
	}

	public function changePasswordStore(Request $request)
	{
		$user = Auth::user();

		$validator = Validator::make($request->all(), [
				'password'	=> 'required|passcheck:' . $user->password,
				'new_password' => 'required|confirmed|min:6'
			], [
				'password.required' => 'Kata Sandi tidak boleh kosong',
	            'password.passcheck' => 'Kata Sandi tidak cocok',
	            'new_password.required' => 'Kata Sandi Baru tidak boleh kosong',
	            'new_password.confirmed' => 'Konfirmasi Kata Sandi tidak cocok',
	            'new_password.min' => 'Kata Sandi baru minimal 6 karakter',
			]);

		if( $validator->fails() )
		{
            return redirect()
            			->back()
                        ->withErrors($validator);
        }

        $update = User::where('id', $user->id)->update([
			'password' => Hash::make($request->new_password)
		]);

		if( $update ) return redirect()->back()->with('alert-success', 'Berhasil memperbarui kata sandi');

		return redirect()->back()->with('alert-error', 'Gagal memperbarui kata sandi');
	}
}