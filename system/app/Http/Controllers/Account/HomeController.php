<?php

namespace App\Http\Controllers\Account;

use Session, Auth;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index(Request $request) 
	{
        
		$user = Auth::user();
		$totalCount = Post::where('user_id', $user->id)->count();
		$reviewCount = Post::where('user_id', $user->id)->where('status', Post::STATUS_REVIEW)->count();
		$approvedCount = Post::where('user_id', $user->id)->where('status', Post::STATUS_PUBLIC)->count();
		$rejectedCount = Post::where('user_id', $user->id)->where('status', Post::STATUS_REJECTED)->count();

		$status = is_null($request->input('status')) ? 'all' : $request->input('status');
		$search_by = empty($request->input('search_by')) ? 'all' : $request->input('search_by');
		$keyword = empty($request->input('keyword')) ? ' ' : $request->input('keyword');

        
		$posts = Post::where('user_id', $user->id);
		
		if( !is_null($status) && $status != 'all' )
		{
			$posts = $posts->where('status', $status);
		}

		switch($search_by)
        {
            case "nama":
                $searchText = '%'.str_replace(' ', '%', $keyword).'%';
                $column = 'nama';
                $posts = $posts->where($column, 'like', $searchText);
                break;

            case "nomor_rekening":
                $searchText = '%"nomor_rekening":"'.str_replace(' ', '', $keyword).'"%';
                $column = 'rekening_bank';
                $posts = $posts->where($column, 'like', $searchText);
                break;

            case "nomor_telepon":
                $searchText = '%"'.str_replace(' ', '', $keyword).'"%';
                $column = 'nomor_telepon';
                $posts = $posts->where($column, 'like', $searchText);
                break;

            case "email":
                $searchText = '"'.str_replace(' ', '', $keyword).'"';
                $column = 'email';
                $posts = $posts->where($column, 'like', $searchText);
                break;

            case "media_sosial":
                $searchText = '%'.str_replace(' ', '', $keyword).'%';
                $column = 'media_sosial';
                $posts = $posts->where($column, 'like', $searchText);
                break;

            default:
                break;
        }
        
        $limit = 10;
        $page = $request->page ?? 1 ;
        $offset = ($page * $limit) - ($limit) ;
        $jumpos = $posts->count();
        $jumpage = intval(ceil($jumpos / $limit));
		$posts = $posts->offset($offset)->limit($limit)->get();
		
		return view('account.home', compact('totalCount', 'reviewCount', 'approvedCount', 'rejectedCount', 'posts', 'request', 'jumpage', 'page'));
	}
}