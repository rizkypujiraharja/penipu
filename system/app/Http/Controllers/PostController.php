<?php

namespace App\Http\Controllers;

use DB, Validator, Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;

class PostController extends Controller
{
	public function view(Request $request, $id)
	{
		$id = intval($id);
		$post = Post::where('id', $id)->where('status', Post::STATUS_PUBLIC)->firstOrFail();
		$poster = $post->__user;
		$comments = $post->__comments;
		$nama = json_decode($post->nama);
		$rekening_bank = json_decode($post->rekening_bank);
		$nomor_telepon = json_decode($post->nomor_telepon);
		$email = json_decode($post->email);
		$media_sosial = json_decode($post->media_sosial);
		$screenshot = json_decode($post->screenshot);

		return view('post.view', compact('post', 'poster', 'nama', 'rekening_bank', 'nomor_telepon', 'email', 'media_sosial', 'screenshot', 'comments'));
	}

	public function ajaxLoadComment(Request $request, $id)
	{
		$id = intval($id);

		$validation = Validator::make($request->all(), [
				'offset'	=> 'required|numeric|min:0',
				'limit'		=> 'required|numeric|min:1',
				'sort'		=> 'required|string'
			]);

		if( $validation->fails() || ($id <= 0) )
		{
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan memuat komentar. Request tidak valid',
				'data'			=> []
				]);
		}

		$offset = intval($request->offset);
		$limit = intval($request->limit);
		$limit = ($limit > 0 && $limit <= 10) ? $limit : 10;
		$sort = in_array($request->sort, ['asc', 'desc']) ? $request->sort : 'desc';

		$post = Post::find($id);

		if( !$post )
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'ID post tidak valid',
				'data'			=> []
				]);

		$comments = Comment::where('post_id', $post->id)
							->orderBy('id', $sort)
							->offset($offset)
							->limit($limit)
							->get();

		return response()->json([
				'success'		=> true,
				'error_message'	=> '',
				'data'			=> $comments
				]);
	}

	public function ajaxNewComment(Request $request, $id)
	{
		$id = intval($id);

		$validation = Validator::make($request->all(), [
				'content'	=> 'required|string'
			]);

		if( $validation->fails() )
		{
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan. Komentar tidak boleh kosong',
				'data'			=> null
				]);
		}
		elseif( $id <= 0 )
		{
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Invalid request',
				'data'			=> null
				]);
		}

		DB::beginTransaction();

		try
		{
			$user = Auth::user();

			/*
			if( $user->verified_member != 1 )
				throw new \Exception("Anda tidak memiliki izin untuk mengirim komentar. Silahkan lakukan verifikasi akun terlebih dahulu");
			*/

			$comment = new Comment();
			$comment->user_id = $user->id;
			$comment->post_id = $id;
			$comment->content = htmlentities($request->content, ENT_QUOTES);
			if( !$comment->save() )
				throw new \Exception("Gagal mengirim komentar");

			DB::commit();

			return response()->json([
				'success'		=> true,
				'error_message'	=> '',
				'data'			=> [
									"comment"	=> Comment::find($comment->id),
									"user"		=> $user,
									"other"		=> [
													"unix_timestamp"		=> time(),
													"human_readable_timestamp"	=> strftime('%d %b %Y %H:%M', time())
													]
								]
				]);
		}
		catch(\PDOException $pdo)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan sistem',
				'data'			=> null
				]);
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> $e->getMessage(),
				'data'			=> null
				]);
		}
	}

	public function ajaxUpdateComment(Request $request, $id)
	{
		$id = intval($id);

		$validation = Validator::make($request->all(), [
				'comment_id'	=> 'required|numeric|min:1',
				'content'		=> 'required|string'
			]);

		if( $validation->fails() || ($id <= 0) )
		{
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan. Request tidak valid',
				'data'			=> null
				]);
		}

		DB::beginTransaction();

		try
		{
			$user = Auth::user();

			$post = Post::find($id);

			if( !$post )
				throw new \Exception("Gagal memperbarui komentar. ID postingan tidak valid");


			$comment = Comment::where('id', $request->comment_id)
								->where('post_id', $post->id)
								->where('user_id', $user->id)
								->where('deleted', 0)
								->first();

			if( !$comment )
				throw new \Exception("Gagal memperbarui komentar. ID komentar tidak valid atau komentar telah dihapus");

			$comment->content = htmlentities($request->content, ENT_QUOTES);

			if( !$comment->save() )
				throw new \Exception("Gagal memperbarui komentar");

			DB::commit();

			return response()->json([
				'success'		=> true,
				'error_message'	=> '',
				'data'			=> Comment::find($comment->id)
				]);
		}
		catch(\PDOException $pdo)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan sistem',
				'data'			=> null
				]);
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> $e->getMessage(),
				'data'			=> null
				]);
		}
	}

	public function ajaxDeleteComment(Request $request, $id)
	{
		$id = intval($id);

		$validation = Validator::make($request->all(), [
				'comment_id'	=> 'required|numeric|min:1'
			]);

		if( $validation->fails() || ($id <= 0) )
		{
			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan. Request tidak valid',
				'data'			=> null
				]);
		}

		DB::beginTransaction();

		try
		{
			$user = Auth::user();

			$post = Post::find($id);

			if( !$post )
				throw new \Exception("Gagal menghapus komentar. ID postingan tidak valid");

			$deleteComment = Comment::where('id', $request->comment_id)
								->where('post_id', $post->id)
								->where('user_id', $user->id)
								->where('deleted', 0)
								->update([
									'deleted'			=> 1,
									'delete_by_user'	=> 1
									]);

			if( !$deleteComment )
				throw new \Exception("Gagal menghapus komentar. ID komentar tidak valid atau komentar telah dihapus");

			DB::commit();

			return response()->json([
				'success'		=> true,
				'error_message'	=> '',
				'data'			=> Comment::find($request->comment_id)
				]);
		}
		catch(\PDOException $pdo)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> 'Terjadi kesalahan sistem',
				'data'			=> null
				]);
		}
		catch(\Exception $e)
		{
			DB::rollBack();

			return response()->json([
				'success'		=> false,
				'error_message'	=> $e->getMessage(),
				'data'			=> null
				]);
		}
	}
}