<?php

namespace App\Http\Controllers;

use DB, Validator;
use App\Models\Post;
use App\Helpers\Crypto;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**s
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postCount = Post::whereNotIn('status', [Post::STATUS_DRAFT, Post::STATUS_REJECTED])->count();
        $blackListCount = Post::where('status', Post::STATUS_PUBLIC)->where('rekening_bank', '!=', '[]')->count();
        $kerugian = Post::where('status', Post::STATUS_PUBLIC)->sum('jumlah_kerugian');
        return view('home', compact('postCount', 'blackListCount', 'kerugian'));
    }

    public function ajaxSearch(Request $request)
    {
        $search_by = $request->input('search_by');
        $keyword = $request->input('keyword');
        $keyword = preg_replace('!\s+!', ' ', $keyword);
        $keyword = trim($keyword);

        switch($search_by)
        {
            case "nama":
                $searchText = '%'.str_replace(' ', '%', $keyword).'%';
                $column = 'nama';
                break;

            case "rekening":
                $searchText = '%"nomor_rekening":"'.str_replace(' ', '', $keyword).'"%';
                $column = 'rekening_bank';
                break;

            case "telepon":
                $searchText = '%"'.str_replace(' ', '', $keyword).'"%';
                $column = 'nomor_telepon';
                break;

            case "email":
                $searchText = '%"'.str_replace(' ', '', $keyword).'"%';
                $column = 'email';
                break;

            case "medsos":
                $searchText = '%'.str_replace(' ', '', $keyword).'%';
                $column = 'media_sosial';
                break;

            default:
                return response()->json([
                    'success'       => false,
                    'message'       => 'Kategori pencarian tidak valid. Mohon periksa kembali atau gunakan kategori lain',
                    'result_count'  => 0
                    ]);
                break;
        }
        
        return response()->json([
            'success'       => true,
            'message'       => '',
            'result_count'  => Post::where('status', Post::STATUS_PUBLIC)->where($column, 'like', $searchText)->count()
            ]);
    }

    public function search(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'by'        => 'required|string|regex:/^[a-zA-Z\_]/i',
            'keyword'   => 'required|string',
            'offset'    => 'numeric',
            'limit'     => 'numeric|max:20'
            ], [
            'by.required'       => '',
            'by.string'         => '',
            'by.regex'          => '',
            'keyword.required'  => '',
            'keyword.string'    => '',
            'offset.numeric'    => '',
            'limit.numeric'     => '',
            'limit.max'         => ''
            ]);

        if( $validation->fails() )
            return redirect()->route('home')->with('flash-error', 'Beberapa data tidak valid, silahkan coba lagi');

        $by = $request->by;
        $keyword = $request->keyword;
        $keyword = preg_replace('!\s+!', ' ', $keyword);
        $keyword = trim($keyword);

        switch($by)
        {
            case "nama":
                $searchText = '%'.str_replace(' ', '%', $keyword).'%';
                $column = 'nama';
                break;

            case "rekening":
                $searchText = '%"nomor_rekening":"'.str_replace(' ', '', $keyword).'"%';
                $column = 'rekening_bank';
                break;

            case "telepon":
                $searchText = '%"'.str_replace(' ', '', $keyword).'"%';
                $column = 'nomor_telepon';
                break;

            case "email":
                $searchText = '%"'.str_replace(' ', '', $keyword).'"%';
                $column = 'email';
                break;

            case "medsos":
                $searchText = '%'.str_replace(' ', '', $keyword).'%';
                $column = 'media_sosial';
                break;

            default:
                return redirect()->route('home')->with('flash-error', 'Beberapa data tidak valid, silahkan coba lagi');
                break;
        }

        $sql = Post::where('status', Post::STATUS_PUBLIC)
                        ->where($column, 'like', $searchText)
                        ->orderBy('id', 'desc');


        $limit = 10;
        $page = $request->page ?? 1 ;
        $offset = ($page * $limit) - ($limit) ;
        $jumpos = $sql->count();
        $jumpage = intval(ceil($jumpos / $limit));
        $posts = $sql->offset($offset)->limit($limit)->get();
        $jumpostinpage = $offset + $posts->count();

        return view('search', compact('by', 'keyword', 'posts', 'offset', 'jumpostinpage', 'request', 'jumpos', 'jumpage', 'page'));
    }
}
