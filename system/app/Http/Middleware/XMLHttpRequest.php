<?php

namespace App\Http\Middleware;

use Closure;

class XMLHttpRequest
{
    # verify if request is from XMLHttpRequest
    public function handle($request, Closure $next)
    {
        $rw = $request->header("x-requested-with");
        if( strtoupper($rw) != 'XMLHTTPREQUEST' )
        {
            abort(403);
        }

        return $next($request);
    }
}