<?php

namespace App\Http\Middleware;

use Closure;

class PreventBot
{
	public function handle($request, Closure $next)
	{
		$ip = clientIP();
		$ua = $request->header('user-agent');

		if( empty($ip) || empty($ua) )
		{
			abort(400);
		}
		elseif( $this->blockedUA($ua) || $this->blockedIP($ip) )
		{
			abort(403);
		}

		return $next($request);
	}

	private function blockedUA($ua)
	{
		$list = [
			'zmeu'
		];

		if( empty($ua) || empty($list) ) return false;

		if( preg_match('/('.implode('|', $list).')/i', $ua) ) return true;

		return false;
	}

	private function blockedIP($ip)
	{
		$list = [
			//
		];

		if( empty($ip) || empty($list) ) return false;

		if( preg_match('/('.implode('|', $list).')/i', $ip) ) return true;

		return false;
	}
}