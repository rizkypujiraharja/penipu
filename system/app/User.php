<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'verified_email', 'alamat', 'kota', 'provinsi', 'kode_pos', 'verified_member', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const UNVERIFIED = 0;
    const VERIFIED = 1;

    public function __posts()
    {
        return $this->hasMany('App\Models\Post', 'user_id');
    }

    public function __comments()
    {
        return $this->hasMany('App\Models\Comment', 'user_id');
    }

    public function __verificationData()
    {
        return $this->hasOne('App\Models\UserVerification', 'user_id');
    }
}